##  Dianara - A Pump.io client
##  Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 2 of the License, or
##  (at your option) any later version.
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the
##  Free Software Foundation, Inc.,
##  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
##
## -------------------------------------------------
## Project created by QtCreator
## -------------------------------------------------

message("Generating Makefile for Dianara... $$escape_expand(\\n)\
Using $$_FILE_$$escape_expand(\\n)")


QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4) {
    message("Building with Qt 5: v$$QT_VERSION $$escape_expand(\\n)")

    warning(">>> This is currently NOT SUPPORTED.")
    warning(">>> Building should succeed, but the generated binary will")
    warning(">>> probably crash on startup, unless you have qoauth-qt5")
    warning(">>>")
    warning(">>> To build with Qt 4, you probably need to use qmake-qt4 instead\
$$escape_expand(\\n)")
    QT += widgets
}

lessThan(QT_MAJOR_VERSION, 5) {
    message("Building with Qt 4: v$$QT_VERSION")
    LIBS += -lqjson
}



TARGET = dianara

TEMPLATE = app


SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/configdialog.cpp \
    src/notifications.cpp \
    src/post.cpp \
    src/timeline.cpp \
    src/publisher.cpp \
    src/composer.cpp \
    src/timestamp.cpp \
    src/contactcard.cpp \
    src/mischelpers.cpp \
    src/pumpcontroller.cpp \
    src/imageviewer.cpp \
    src/minorfeed.cpp \
    src/profileeditor.cpp \
    src/accountdialog.cpp \
    src/audienceselector.cpp \
    src/commenterblock.cpp \
    src/comment.cpp \
    src/minorfeeditem.cpp \
    src/listsmanager.cpp \
    src/asobject.cpp \
    src/asactivity.cpp \
    src/filtereditor.cpp \
    src/peoplewidget.cpp \
    src/logviewer.cpp \
    src/asperson.cpp \
    src/avatarbutton.cpp \
    src/colorpicker.cpp \
    src/filterchecker.cpp \
    src/contactmanager.cpp \
    src/contactlist.cpp \
    src/downloadwidget.cpp \
    src/proxydialog.cpp \
    src/helpwidget.cpp \
    src/fontpicker.cpp \
    src/groupsmanager.cpp \
    src/globalobject.cpp \
    src/pageselector.cpp \
    src/hclabel.cpp \
    src/userposts.cpp \
    src/emailchanger.cpp \
    src/siteuserslist.cpp \
    src/firstrunwizard.cpp \
    src/bannernotification.cpp

HEADERS  += src/mainwindow.h \
    src/configdialog.h \
    src/notifications.h \
    src/post.h \
    src/timeline.h \
    src/publisher.h \
    src/composer.h \
    src/timestamp.h \
    src/contactcard.h \
    src/mischelpers.h \
    src/pumpcontroller.h \
    src/imageviewer.h \
    src/minorfeed.h \
    src/profileeditor.h \
    src/accountdialog.h \
    src/audienceselector.h \
    src/commenterblock.h \
    src/comment.h \
    src/minorfeeditem.h \
    src/listsmanager.h \
    src/asobject.h \
    src/asactivity.h \
    src/filtereditor.h \
    src/peoplewidget.h \
    src/logviewer.h \
    src/asperson.h \
    src/avatarbutton.h \
    src/colorpicker.h \
    src/filterchecker.h \
    src/contactmanager.h \
    src/contactlist.h \
    src/downloadwidget.h \
    src/proxydialog.h \
    src/helpwidget.h \
    src/fontpicker.h \
    src/groupsmanager.h \
    src/globalobject.h \
    src/pageselector.h \
    src/hclabel.h \
    src/userposts.h \
    src/emailchanger.h \
    src/siteuserslist.h \
    src/firstrunwizard.h \
    src/bannernotification.h



# D-Bus available, include D-Bus interface and enable dbus-based notifications
!win32 {
    message("Building with D-Bus support")
    QT += dbus

    SOURCES += src/dbusinterface.cpp
    HEADERS  += src/dbusinterface.h
}


# SOURCE_DATE_EPOCH is read from environment, to enable reproducible builds in Debian
source_date_epoch = $$(SOURCE_DATE_EPOCH)
!isEmpty(source_date_epoch) {
    message("Creating a reproducible build (avoiding hardcoded timestamps) \
because SOURCE_DATE_EPOCH is defined: $$(SOURCE_DATE_EPOCH)")
    DEFINES += REPRODUCIBLEBUILD
}



OTHER_FILES += \
    CHANGELOG \
    README \
    dianara.desktop \
    INSTALL \
    TODO \
    BUGS \
    TRANSLATING \
    manual/dianara.1


TRANSLATIONS += translations/dianara_es.ts \
    translations/dianara_ca.ts \
    translations/dianara_gl.ts \
    translations/dianara_eu.ts \
    translations/dianara_fr.ts \
    translations/dianara_it.ts \
    translations/dianara_de.ts \
    translations/dianara_pt.ts \
    translations/dianara_ru.ts \
    translations/dianara_pl.ts \
    translations/dianara_he.ts \
    translations/dianara_EMPTY.ts

RESOURCES += dianara.qrc

LIBS += -lqoauth -lmagic # to use libmagic (and QOauth in Debian)

CONFIG += oauth crypto # To use QOAuth (Debian also needs 'crypto')

win32 { # Console support under mswin
    CONFIG += console
}


## This is here so the makefile has a 'make install' target
target.path = /usr/bin/
INSTALLS += target


## TMP/FIXME added for Debian (Debian's libqoauth-dev bug)
INCLUDEPATH += /usr/include/QtOAuth


message("$$escape_expand(\\n\\n\\n)\
Makefile done!$$escape_expand(\\n\\n)\
If you're building the binary, you can run 'make' now. $$escape_expand(\\n)")
