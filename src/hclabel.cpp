/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "hclabel.h"


HClabel::HClabel(QString initialText,
                 QWidget *parent) : QLabel(parent)
{
    this->setWordWrap(true);
    this->setAutoFillBackground(true);
    this->setTextFormat(Qt::RichText);

    this->setBaseText(initialText);

    this->setHighlighted(false); // Default
    this->setExpanded(false);


    this->toggleTimer = new QTimer(this);
    toggleTimer->setSingleShot(true);
    connect(toggleTimer, SIGNAL(timeout()),
            this, SLOT(toggleContent()));

    qDebug() << "HClabel created";
}

HClabel::~HClabel()
{
    qDebug() << "HClabel destroyed";
}


void HClabel::setHighlighted(bool highlighted)
{
    if (highlighted)
    {
        // Constant highlighting
        this->setStyleSheet("QLabel "
                            "{ color: palette(highlighted-text);               "
                            "  background-color: qlineargradient(spread:pad,   "
                            "                     x1:0, y1:0, x2:0, y2:1,      "
                            "                     stop:0   rgba(0, 0, 0, 0),   "
                            "                     stop:0.3 palette(highlight), "
                            "                     stop:0.7 palette(highlight), "
                            "                     stop:1   rgba(0, 0, 0, 0));  "
                            "  border-radius: 4px                              "
                            "}"
                            "QLabel:hover "
                            "{ color: palette(highlighted-text);    "
                            "  background-color: palette(highlight);"
                            "  border-radius: 4px                   "
                            "}");
    }
    else
    {
        // Highlight only on mouse hover
        this->setStyleSheet("QLabel "
                            "{ background-color: transparent }"
                            "QLabel:hover "
                            "{ color: palette(highlighted-text);    "
                            "  background-color: palette(highlight);"
                            "  border-radius: 4px                   "
                            "}");
    }
}



void HClabel::setBaseText(QString text)
{
    this->baseText = text;

    if (extendedText.isEmpty() || !expanded)
    {
        this->setText(this->baseText);
    }
    else
    {
        this->setText(baseText
                      + "<hr>"
                      + extendedText
                      + "<hr>");
    }
}


void HClabel::setExtendedText(QString text)
{
    this->baseText = this->text(); // Save it
    this->extendedText = text.trimmed();
}


void HClabel::setExpanded(bool isExpanded)
{
    this->expanded = isExpanded;
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////// SLOTS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void HClabel::toggleContent()
{
    if (expanded)
    {
        this->setText(baseText);
    }
    else
    {
        if (!this->extendedText.isEmpty())
        {
            this->setText(baseText
                          + "<hr>"
                          + extendedText
                          + "<hr>");
        }
    }

    this->setExpanded(!expanded);
}


//////////////////////////////////////////////////////////////////////////////
/////////////////////////////// PROTECTED ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void HClabel::mousePressEvent(QMouseEvent *event)
{
    toggleTimer->start(200);

    // Don't emit the signal for now... we'll see if we need it
    //emit clicked();

    event->ignore(); // Let the click pass through to the parent
}
