/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef LISTSMANAGER_H
#define LISTSMANAGER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QTreeWidget>
#include <QGroupBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QMessageBox>

#include <QInputDialog> // TMP!!

#include <QDebug>

#include "pumpcontroller.h"
#include "peoplewidget.h"


class ListsManager : public QWidget
{
    Q_OBJECT

public:
    explicit ListsManager(PumpController *pumpController,
                          QWidget *parent = 0);
    ~ListsManager();

    void setListsList(QVariantList listsList);

    QTreeWidgetItem *createPersonItem(QString id,
                                      QString name,
                                      QString avatarFile);

signals:


public slots:
    void setPersonsInList(QVariantList personList, QString listUrl);
    void createList();
    void deleteList();
    void enableDisableCreateButton(QString listName);

    void enableDisableDeleteButtons();

    void showAddPersonDialog();
    void addPerson(QIcon icon, QString contactString, QString contactUrl);
    void removePerson();

    void addPersonItemToList(QString personId, QString personName,
                             QString avatarUrl);
    void removePersonItemFromList(QString personId);


private:
    PumpController *pController;

    QVBoxLayout *mainLayout;
    QHBoxLayout *buttonsLayout;

    QTreeWidget *listsTreeWidget;
    QPushButton *deleteListButton;

    QPushButton *addPersonButton;
    QPushButton *removePersonButton;

    PeopleWidget *peopleWidget;

    QGroupBox *newListGroupbox;
    QHBoxLayout *groupboxMainLayout;
    QVBoxLayout *groupboxLeftLayout;

    QLineEdit *newListNameLineEdit;
    QTextEdit *newListDescTextEdit;
    QPushButton *createListButton;

    QStringList personListsUrlList;
};


#endif // LISTSMANAGER_H
