/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QWidget>
#include <QLabel>
#include <QIcon>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QDesktopWidget>
#include <QMenu>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QMovie>
#include <QScrollArea>

#include <QDebug>

#include "mischelpers.h"

class ImageViewer : public QWidget
{
    Q_OBJECT

public:
    explicit ImageViewer(QString url, QSize imageSize,
                         QString title, QString suggestedFN,
                         bool isAnimated,
                         QWidget *parent = 0);
    ~ImageViewer();
    void createContextMenu();
    void loadImage(QString url, QSize expectedSize);
    void drawImage();
    void toggleZoomButtons();
    void updateButtons();


signals:


public slots:
    void reloadImage(QString url);
    void onImageFailed(QString url);

    void saveImage();
    void restartAnimation();

    void zoomToFit();
    void zoomIn();
    void zoomOut();


protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void hideEvent(QHideEvent *event);
    virtual void resizeEvent(QResizeEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *event);


private:
    QVBoxLayout *mainLayout;
    QLabel *imageLabel;
    QScrollArea *scrollArea;

    QHBoxLayout *buttonsLayout;
    QPushButton *saveImageButton;
    QPushButton *restartButton;

    QHBoxLayout *fitFullLayout;
    QPushButton *fitButton;
    QPushButton *fullButton;
    QPushButton *zoomInButton;
    QPushButton *zoomOutButton;
    QLabel *infoLabel;
    QPushButton *closeButton;

    QMenu *contextMenu;
    QAction *saveAction;
    QAction *closeAction;

    QString imageUrl;

    QPixmap originalPixmap;
    QString originalFileURI;
    bool imageIsAnimated;
    QMovie *movie;
    QString suggestedFilename;

    bool fitToWindow;
    float zoomLevel;
};

#endif // IMAGEVIEWER_H
