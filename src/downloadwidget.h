/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef DOWNLOADWIDGET_H
#define DOWNLOADWIDGET_H

#include <QFrame>
#include <QHBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>

#include <QDebug>

#include "pumpcontroller.h"


class DownloadWidget : public QFrame
{
    Q_OBJECT

public:
    explicit DownloadWidget(QString fileUrl,
                            QString suggestedFN,
                            PumpController *pumpController,
                            QWidget *parent = 0);
    ~DownloadWidget();

    void resetWidget();


signals:


public slots:
    void downloadAttachment();
    void cancelDownload();
    void completeDownload(QString url);
    void onDownloadFailed(QString url);

    void storeFileData();
    void updateProgressBar(qint64 received, qint64 total);


private:
    QHBoxLayout *mainLayout;
    QLabel *infoLabel;
    QProgressBar *progressBar;
    QPushButton *downloadButton;
    QPushButton *cancelButton;

    QString fileUrl;
    QString suggestedFilename;

    PumpController *pController;
    QNetworkReply *networkReply;
    QFile downloadedFile;
    bool downloading;
};

#endif // DOWNLOADWIDGET_H
