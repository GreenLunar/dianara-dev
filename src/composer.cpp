/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "composer.h"


Composer::Composer(GlobalObject *globalObject, bool forPublisher,
                   QWidget *parent) : QTextEdit(parent)
{
    this->globalObj = globalObject;
    this->forPublisher = forPublisher;

    this->setAcceptRichText(true);
    this->setTabChangesFocus(true);

    QFont startConversationFont;
    startConversationFont.setPointSize(startConversationFont.pointSize() - 2);

    startConversationLabel = new QLabel(tr("Click here or press Control+N "
                                           "to post a note..."),
                                        this);
    startConversationLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    startConversationLabel->setFont(startConversationFont);

    // A menu to insert some Unicode symbols
    symbolsMenu = new QMenu(tr("Symbols"), this);
    symbolsMenu->setIcon(QIcon::fromTheme("character-set"));
    symbolsMenu->addAction(QString::fromUtf8("\342\230\272")); // Smiling face
    symbolsMenu->addAction(QString::fromUtf8("\342\230\271")); // Sad face
    symbolsMenu->addAction(QString::fromUtf8("\342\231\245")); // Heart
    symbolsMenu->addAction(QString::fromUtf8("\342\231\253")); // Musical note
    symbolsMenu->addAction(QString::fromUtf8("\342\230\225")); // Coffee
    symbolsMenu->addAction(QString::fromUtf8("\342\234\224")); // Check mark
    symbolsMenu->addAction(QString::fromUtf8("\342\234\230")); // Ballot X
    symbolsMenu->addAction(QString::fromUtf8("\342\230\205")); // Black star
    symbolsMenu->addAction(QString::fromUtf8("\342\254\205")); // Arrow to the left
    symbolsMenu->addAction(QString::fromUtf8("\342\236\241")); // Arrow to the right
    symbolsMenu->addAction(QString::fromUtf8("\342\231\273")); // Recycling symbol
    symbolsMenu->addAction(QString::fromUtf8("\342\210\236")); // Infinity
    connect(symbolsMenu, SIGNAL(triggered(QAction*)),
            this, SLOT(insertSymbol(QAction*)));


    toolsMenu = new QMenu(tr("Formatting"), this);
    toolsMenu->addAction(QIcon::fromTheme(""),
                         tr("Normal"),
                         this,
                         SLOT(makeNormal()));
    toolsMenu->addAction(QIcon::fromTheme("format-text-bold"),
                         tr("Bold"),
                         this,
                         SLOT(makeBold()),
                         QKeySequence("Ctrl+B"));
    toolsMenu->addAction(QIcon::fromTheme("format-text-italic"),
                         tr("Italic"),
                         this,
                         SLOT(makeItalic()),
                         QKeySequence("Ctrl+I"));
    toolsMenu->addAction(QIcon::fromTheme("format-text-underline"),
                         tr("Underline"),
                         this,
                         SLOT(makeUnderline()),
                         QKeySequence("Ctrl+U"));
    toolsMenu->addAction(QIcon::fromTheme("format-text-strikethrough"),
                         tr("Strikethrough"),
                         this,
                         SLOT(makeStrikethrough()));

    toolsMenu->addSeparator();

    toolsMenu->addAction(QIcon::fromTheme("format-font-size-more"),
                         tr("Header"),
                         this,
                         SLOT(makeHeader()),
                         QKeySequence("Ctrl+H"));
    toolsMenu->addAction(QIcon::fromTheme("format-list-unordered"),
                         tr("List"),
                         this,
                         SLOT(makeList()));
    toolsMenu->addAction(QIcon::fromTheme("insert-table"),
                         tr("Table"),
                         this,
                         SLOT(makeTable()));
    toolsMenu->addAction(QIcon::fromTheme("format-justify-fill"),
                         tr("Preformatted block"),
                         this,
                         SLOT(makePreformatted()));
    toolsMenu->addAction(QIcon::fromTheme("format-text-italic"),
                         tr("Quote block"),
                         this,
                         SLOT(makeQuote()),
                         QKeySequence("Ctrl+O"));

    toolsMenu->addSeparator();

    toolsMenu->addAction(QIcon::fromTheme("insert-link"),
                         tr("Make a link"),
                         this,
                         SLOT(makeLink()),
                         QKeySequence("Ctrl+L"));
    toolsMenu->addAction(QIcon::fromTheme("insert-image"),
                         tr("Insert an image from a web site"),
                         this,
                         SLOT(insertImage()),
                         QKeySequence("Ctrl+P"));
    toolsMenu->addAction(QIcon::fromTheme("insert-horizontal-rule"),
                         tr("Insert line"),
                         this,
                         SLOT(insertLine()),
                         QKeySequence("Ctrl+Shift+L"));

    toolsMenu->addSeparator();

    toolsMenu->addMenu(symbolsMenu);


    toolsButton = new QPushButton(QIcon::fromTheme("format-list-ordered",
                                                   QIcon(":/images/button-configure.png")),
                                  tr("&Format",
                                     "Button for text formatting and related options"),
                                  this);
    toolsButton->setMenu(toolsMenu);
    toolsButton->setToolTip("<b></b>"
                            + tr("Text Formatting Options"));


    // Extra action for the context menu, paste as plaintext
    this->pastePlaintextAction = new QAction(QIcon::fromTheme("paste"),
                                             tr("Paste Text Without Formatting"),
                                             this);

    pastePlaintextAction->setShortcut(QKeySequence("Ctrl+Shift+V"));
    connect(pastePlaintextAction, SIGNAL(triggered()),
            this, SLOT(pasteAsPlaintext()));

    // Add action to this object, in order for the shortcut to work
    // Otherwise, the action isn't available until the context menu is present
    this->addAction(pastePlaintextAction);


    // Nick completion stuff
    nickCompleter = new QCompleter(globalObj->getNickCompletionModel(),
                                   this);
    nickCompleter->setWidget(this);
    nickCompleter->setMaxVisibleItems(15);
    nickCompleter->setCompletionColumn(0);
    nickCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    nickCompleter->setModelSorting(QCompleter::CaseSensitivelySortedModel); // TMP; FIXME
    connect(nickCompleter, SIGNAL(activated(QModelIndex)),
            this, SLOT(insertCompletedNick(QModelIndex)));

    popupTableView = new QTableView(this);
    popupTableView->horizontalHeader()->hide();
    popupTableView->verticalHeader()->hide();
    popupTableView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    popupTableView->setAlternatingRowColors(true);
    popupTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    popupTableView->setSelectionMode(QAbstractItemView::SingleSelection);
    popupTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    nickCompleter->setPopup(popupTableView);

    mainLayout = new QHBoxLayout();

    if (this->forPublisher)  // Publisher mode
    {
        this->setToolTip("<b></b>"
                         + tr("Type a message here to post it"));

        mainLayout->addWidget(startConversationLabel, 1, Qt::AlignLeft | Qt::AlignVCenter);
    }
    else                     // or Commenter mode
    {
        this->setToolTip("<b></b>"
                         + tr("Type a comment here"));

        startConversationLabel->hide(); // Since it has a parent but it's not used, hide it
    }
    this->setLayout(mainLayout);

    qDebug() << "Composer box created";
}


Composer::~Composer()
{
    qDebug() << "Composer box destroyed";
}



void Composer::erase()
{
    this->clear();

    if (this->forPublisher)
    {
        startConversationLabel->show();
    }
}


void Composer::insertLink(QString url, QString title)
{
    bool prettyLink = true;
    if (title.isEmpty())
    {
        prettyLink = false;
        title = url;
    }

    this->insertHtml("<a href=\"" + url + "\">"
                     + title + "</a>");

    // Space after, when there's no text after the link,
    // or the link is inserted without selecting text before,
    // so what the user types next is regular text
    if (this->textCursor().atEnd() || !prettyLink)
    {
        this->insertHtml("&nbsp;");
        // Could use this->makeNormal(), but has some drawbacks
    }
}


void Composer::requestCompletion(QString partialNick)
{
    nickCompleter->setCompletionPrefix(partialNick);
    popupTableView->ensurePolished();

    popupTableView->resizeColumnsToContents();
    popupTableView->resizeRowsToContents();
    popupTableView->ensurePolished();

    int tableWidth = popupTableView->columnWidth(0)
                     + popupTableView->columnWidth(1) + 2;
    if (tableWidth > this->width() + 2)
    {
        tableWidth = this->width() + 2;
        popupTableView->setColumnWidth(0, (tableWidth / 4) * 3);
        popupTableView->setColumnWidth(1, tableWidth / 4);
    }

    popupTableView->setMinimumWidth(tableWidth);

    int rows = qMin(popupTableView->model()->rowCount(), 15);
    int rowHeight = popupTableView->rowHeight(0) + 4;
    popupTableView->setMinimumHeight(rows * rowHeight);

    // Popup might get disabled at some point when Composer is used in a Post
    popupTableView->setEnabled(true);

    nickCompleter->complete(QRect(this->cursorRect().x(),
                                  this->cursorRect().y() + 24,
                                  tableWidth, 1));
}


int Composer::getMessageLabelHeight()
{
    // kinda TMP / FIXME
    return (this->startConversationLabel->font().pointSize() * 4) + 4;
}


/*
 * Hide placeholder message
 *
 */
void Composer::hideInfoMessage()
{
    this->startConversationLabel->hide();
}


QPushButton *Composer::getToolsButton()
{
    return this->toolsButton;
}


/*
 * Enable or disable the Ctrl+Shift+V action to paste without format
 *
 * Needed to avoid this conflict between Publisher and Commenters:
 *
 *   QAction::eventFilter: Ambiguous shortcut overload: Ctrl+Shift+V
 *
 */
void Composer::setPlainPasteEnabled(bool state)
{
    this->pastePlaintextAction->setEnabled(state);
}


/*****************************************************************************/
/****************************** PROTECTED ************************************/
/*****************************************************************************/


/*
 * Send a signal when getting focus
 *
 */
void Composer::focusInEvent(QFocusEvent *event)
{
    emit focusReceived(); // inform Publisher() or Commenter() that we have focus
    QTextEdit::focusInEvent(event); // process standard event: allows context menu

    qDebug() << "Composer box got focus";
}



void Composer::keyPressEvent(QKeyEvent *event)
{
    //qDebug() << "Composer::keyPressEvent()";

    // Allow cancelling/accepting the autocompletion popup
    if (this->nickCompleter->popup()->isVisible())
    {
        if (event->key() == Qt::Key_Enter
         || event->key() == Qt::Key_Return
         || event->key() == Qt::Key_Escape)
        {
            event->ignore();
            return;
        }
    }


    // Control+Enter = Send message (post)
    if ((event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
      && event->modifiers() == Qt::ControlModifier)
    {
        qDebug() << "Control+Enter was pressed";
        emit editingFinished();
    }
    else if (event->key() == Qt::Key_Escape)
    {
        qDebug() << "Escape was pressed";
        if (this->toPlainText().isEmpty())
        {
            qDebug() << "There was no text, cancelling post";
            this->cancelPost();
        }
    }
    else if (event->key() == Qt::Key_Up && this->textCursor().atStart())
    {
        qDebug() << "KEYPRESS: atStart(); will focus on TITLE field";
        emit focusTitleRequested();
    }
    else
    {
        QTextEdit::keyPressEvent(event);

        QTextCursor textCursor = this->textCursor();
        textCursor.select(QTextCursor::WordUnderCursor);

        //if (event->key() == Qt::Key_At)
        if (textCursor.document()->characterAt(textCursor.selectionStart() - 1)
            == QChar('@'))
        {
            // Show completer
            this->requestCompletion(textCursor.selectedText());
        }
        else
        {
            // Hide it, if it was visible
            this->nickCompleter->popup()->hide();
        }
    }

    event->accept();
}


/*
 * For custom context menu
 *
 */
void Composer::contextMenuEvent(QContextMenuEvent *event)
{
    this->customContextMenu = this->createStandardContextMenu();

    // tools menu before default context menu
    customContextMenu->insertMenu(customContextMenu->actions().at(0),
                                  toolsMenu);
    customContextMenu->insertSeparator(customContextMenu->actions().at(1));

    // And options added after default context menu
    customContextMenu->addSeparator();


    customContextMenu->addAction(pastePlaintextAction);
    if (this->canPaste())
    {
        pastePlaintextAction->setEnabled(true);
    }
    else
    {
        pastePlaintextAction->setDisabled(true);
    }

    customContextMenu->exec(event->globalPos());

    // FIXME: Possible leak... should delete customContextMenu?
    customContextMenu->deleteLater();

    event->accept();
}


/*
 * Intervene when pasting, so we can turn link-looking text into real
 * HTML links, and turn direct links to images into embedded images
 *
 */
void Composer::insertFromMimeData(const QMimeData *source)
{
    /*
     * Prevent larjona's crash.
     *
     * Apparently the QMessageBox will mess with the 'source' pointer
     * if the contents of the clipboard came from another program.
     * This causes a segfault when selecting "insert as link".
     *
     * To avoid it, all data needed from 'source' will be stored before showing
     * the message box.
     *
     * https://gitlab.com/dianara/dianara-dev/issues/33
     *
     */

    QString pastedHtml = source->html();
    QString pastedText = source->text().trimmed();

    bool sourceHasHtml = source->hasHtml();


    // First, check if it's just a link to an image, to offer embedding it
    if (pastedText.startsWith("http://")
     || pastedText.startsWith("https://"))
    {
        // If link looks like an image, ask the user how to insert it
        if (pastedText.endsWith(".png", Qt::CaseInsensitive)
         || pastedText.endsWith(".jpg", Qt::CaseInsensitive)
         || pastedText.endsWith(".jpeg", Qt::CaseInsensitive)
         || pastedText.endsWith(".gif", Qt::CaseInsensitive))
        {
            int insertionType= QMessageBox::question(this,
                                            tr("Insert as image?"),
                                            tr("The link you are pasting "
                                               "seems to point to an image.")
                                            + "\n\n\n",
                                            tr("Insert as visible image"), // Default option (Enter)
                                            tr("Insert as link"),          // ESC option
                                            "", 0, 1);

            if (insertionType == 0) // Default button, insert as image
            {
                this->insertHtml("<img src=\"" + pastedText + "\" />"
                                 "<br/><br/>");
                return;
            }
        }
    }

    // If it wasn't just an image link, or offer to embed was rejected, continue

    // WARNING: Avoid accessing 'source' at this point, since the QMessageBox
    //          might have messed with it

    QTextDocument textDocument;
    if (sourceHasHtml)
    {
        textDocument.setHtml(pastedHtml);
        qDebug() << "Parsing pasted RICH (HTML) text for links...";
    }
    else
    {
        textDocument.setPlainText(pastedText);
        qDebug() << "Parsing pasted PLAIN text for links...";
    }


    QTextCursor cursor = QTextCursor(&textDocument);

    do
    {
        cursor.select(QTextCursor::WordUnderCursor);

        QString currentWord = cursor.selection().toPlainText();

        qDebug() << "WORD:" << currentWord;

        if (currentWord == "http" || currentWord == "https"
         || currentWord == "ftp"  || currentWord == "sftp")
        {
            qDebug() << "Text might be a link";

            qDebug() << "Cursor is anchor? " << cursor.charFormat().isAnchor();
            if (!cursor.charFormat().isAnchor())
            {
                qDebug() << "URL-looking text which isn't a link; "
                            "Replacing with proper link!";

                // Select text until end of pasted text, or a space/line break
                while (cursor.movePosition(QTextCursor::NextCharacter,
                                           QTextCursor::KeepAnchor))
                {
                    if (textDocument.characterAt(cursor.position()).isSpace())
                    {
                        break;
                    }
                }

                QString linkFound = cursor.selection().toPlainText();

                if (linkFound.startsWith("http://")
                 || linkFound.startsWith("https://")
                 || linkFound.startsWith("ftp://")
                 || linkFound.startsWith("sftp://"))
                {
                    // Turn current selection into a link to the same as the visible text
                    QTextCharFormat format = cursor.charFormat();
                    format.setAnchorHref(linkFound);
                    format.setAnchor(true);
                    cursor.setCharFormat(format);

                    qDebug() << "Linkified: " << linkFound;
                }
            }
        }
    } while (cursor.movePosition(QTextCursor::NextWord));


    this->insertHtml(textDocument.toHtml() + " ");
}



/*****************************************************************************/
/******************************** SLOTS **************************************/
/*****************************************************************************/



/*
 * Remove text formatting from selection, bold, italic, etc.
 *
 */
void Composer::makeNormal()
{
    QTextCharFormat charFormat;
    charFormat.clearForeground();
    charFormat.clearBackground();
    this->setCurrentCharFormat(charFormat);

    this->setFocus();
}



/*
 * Make selected text bold
 *
 */
void Composer::makeBold()
{
    //qDebug() << this->textCursor().selectionStart() << " -> "  << this->textCursor().selectionEnd();
    QTextCharFormat charFormat;
    if (this->currentCharFormat().fontWeight() == QFont::Bold)
    {
        charFormat.setFontWeight(QFont::Normal);
    }
    else
    {
        charFormat.setFontWeight(QFont::Bold);
    }
    this->mergeCurrentCharFormat(charFormat);

    this->setFocus(); // give focus back to text editor
}


/*
 * Make selected text italic
 *
 */
void Composer::makeItalic()
{
    QTextCharFormat charFormat;
    charFormat.setFontItalic(!this->currentCharFormat().fontItalic());
    this->mergeCurrentCharFormat(charFormat);

    this->setFocus();
}



/*
 * Underline selected text
 *
 */
void Composer::makeUnderline()
{
    QTextCharFormat charFormat;
    charFormat.setFontUnderline(!this->currentCharFormat().fontUnderline());
    this->mergeCurrentCharFormat(charFormat);

    this->setFocus();
}

/*
 * Strike out selected text
 *
 */
void Composer::makeStrikethrough()
{
    QTextCharFormat charFormat;
    charFormat.setFontStrikeOut(!this->currentCharFormat().fontStrikeOut());
    this->mergeCurrentCharFormat(charFormat);

    this->setFocus();
}

/*
 * Turn the selected text into an <h2> header
 *
 */
void Composer::makeHeader()
{
    QString selectedText = this->textCursor().selectedText();

    if (!selectedText.isEmpty())
    {
        this->textCursor().removeSelectedText();
        this->insertHtml("<h2>" + selectedText + "</h2> ");
    }

    this->setFocus();
}


void Composer::makeList()
{
    QString selectedText = this->textCursor().selectedText();

    if (!selectedText.isEmpty())
    {
        this->textCursor().removeSelectedText();
        this->insertHtml("<ul><li>" + selectedText + "</li></ul><br>");
    }

    this->setFocus();
}


void Composer::makeTable()
{
    const QString dialogTitle = tr("Table Size");

    bool inputOk = false;
    int rows = QInputDialog::getInt(this, dialogTitle,
                                    tr("How many rows (height)?")
                                    + "       " // Make the dialog a little wider than necessary
                                    + QString::fromUtf8("\342\207\225") // up-down arrow
                                    + "\n\n",
                                    5,
                                    1, 10,
                                    1, &inputOk);

    if (inputOk) // Rows dialog wasn't cancelled
    {
        int columns = QInputDialog::getInt(this, dialogTitle,
                                           tr("How many columns (width)?")
                                           + "       "
                                           + QString::fromUtf8("\342\207\224") // left-right arrow
                                           + "\n\n",
                                           4,
                                           1, 10,
                                           1, &inputOk);

        if (inputOk) // Columns dialog wasn't cancelled either
        {
            QTextTableFormat tableFormat;
            tableFormat.setCellPadding(2);
            tableFormat.setCellSpacing(4);

            this->textCursor().insertTable(rows, columns, tableFormat);
        }
    }

    this->setFocus();
}



/*
 * Put selected text into a <pre> block
 *
 */
void Composer::makePreformatted()
{
    QString selectedText = this->textCursor().selectedText();

    if (!selectedText.isEmpty())
    {
        this->textCursor().removeSelectedText();
        this->insertHtml("<pre>" + selectedText + "</pre> ");
    }

    this->setFocus();
}

/*
 * Mark selected as quoted, using <blockquote>
 *
 */
void Composer::makeQuote()
{
    QString selectedText = this->textCursor().selectedText();

    if (!selectedText.isEmpty())
    {
        this->textCursor().removeSelectedText();
        this->insertHtml("&nbsp; "
                         "<blockquote>&ldquo;" + selectedText
                         + "&rdquo;</blockquote>"
                           "<br />");
        this->textCursor().deletePreviousChar(); // Delete undesired extra newline
    }
    // FIXME: Qt's HTML changes <blockquote> into its own formatting
    // so other clients and the web UI might not display it as other people's blockquote's

    this->setFocus();
}




/*
 * Convert selected text into a link
 *
 */
void Composer::makeLink()
{
    QString selectedText = this->textCursor().selectedText();
    QString link;

    bool validLink = false;
    bool dialogOk = true;

    while (!validLink)
    {
        if (selectedText.isEmpty())
        {
            link = QInputDialog::getText(this,
                                         tr("Insert a link"),
                                         tr("Type or paste a web address "
                                            "here.\n"
                                            "You could also select some "
                                            "text first, to turn it into "
                                            "a link.")
                                         + "\n\n",
                                         QLineEdit::Normal,
                                         "http://",
                                         &dialogOk);
        }
        else
        {
            QString shortenedText = MiscHelpers::elidedText(selectedText, 40);

            link = QInputDialog::getText(this,
                                         tr("Make a link from selected text"),
                                         tr("Type or paste a web address "
                                            "here.\n"
                                            "The selected text (%1) will be "
                                            "converted to a link.")
                                         .arg("'" + shortenedText + "'")
                                         + "\n\n",
                                         QLineEdit::Normal,
                                         "http://",
                                         &dialogOk);
        }

        if (!dialogOk)
        {
            this->setFocus();
            return;
        }

        link = link.trimmed(); // Remove possible spaces before or after

        if (link.startsWith("http://") || link.startsWith("https://")
         || link.startsWith("ftp://")  || link.startsWith("sftp://")
         || link.startsWith("mailto:"))
        {
            validLink = true;
        }

        if (!validLink)
        {
            int choice = QMessageBox::warning(this,
                                              tr("Invalid link"),
                                              tr("The text you entered does "
                                                 "not look like a link.")
                                              + "<br/><br/>"
                                              + tr("It should start with "
                                                   "one of these types:",
                                                   "It = the link, from "
                                                   "previous sentence")
                                              + "<br/>"
                                                "<ul>"
                                                "<li>http://</li>"
                                                "<li>https://</li>"
                                                "<li>ftp://</li>"
                                                "<li>sftp://</li>"
                                                "<li>mailto:</li>"
                                                "</ul>"
                                                "<br/><br/><br/>",
                                              tr("&Use it anyway"),
                                              tr("&Enter it again"),
                                              tr("&Cancel link"),
                                              1, 2);
            if (choice == 0)
            {
                validLink = true;
            }
            else if (choice == 2)
            {
                this->setFocus();
                return;
            }
        }
    }

    this->textCursor().removeSelectedText();
    this->insertLink(link, selectedText);


    this->setFocus();
}



/*
 * Insert an image from a URL
 *
 */
void Composer::insertImage()
{
    // FIXME: should make sure there is no text selected

    QString imageUrl;
    imageUrl = QInputDialog::getText(this,
                             tr("Insert an image from a URL"),
                             tr("Type or paste the image address here.\n"
                                "The link must point to the image file directly.")
                             + "\n\n",
                             QLineEdit::Normal,
                             "http://");


    if (!imageUrl.isEmpty())
    {
        if (imageUrl.startsWith("http://")
         || imageUrl.startsWith("https://"))
        {
            this->insertHtml("<img src=\"" + imageUrl + "\" />"
                             "<br/><br/>");
        }
        else
        {
            QMessageBox::warning(this, tr("Error: Invalid URL"),
                                 tr("The address you entered (%1) "
                                    "is not valid.\n"
                                    "Image addresses should begin "
                                    "with http:// or https://").arg(imageUrl));
        }
    }
    else
    {
        qDebug() << "insertImage(): Image URL is empty";
    }

    this->setFocus();
}

/*
 * Insert a horizontal line, <hr>
 *
 */
void Composer::insertLine()
{
    this->insertHtml("<hr><br>");

    this->setFocus();
}



void Composer::insertSymbol(QAction *action)
{
    this->insertPlainText(action->text());
    this->insertPlainText(" ");

    this->setFocus();
}



void Composer::pasteAsPlaintext()
{
    QString subtype("plain");
    QString clipboardContents = QApplication::clipboard()->text(subtype,
                                                                QClipboard::Clipboard);

    this->makeNormal(); // Ensure normal text before inserting
    this->insertPlainText(clipboardContents);
}



/*
 * Insert the selected nick chosen from the autocomplete list.
 * Notify the parent object about it so the user can be added
 * to To/Cc.
 *
 */
void Composer::insertCompletedNick(QModelIndex nickData)
{
    QString nickId = nickData.data(Qt::UserRole + 1).toString();
    QString nickName = nickData.data().toString();
    QString nickUrl = nickData.data(Qt::UserRole + 2).toString();

    // Abort if there's no ID; this can happen if the ID column is clicked
    if (nickId.isEmpty())
    {
        qDebug() << "*** AUTOCOMPLETER: ID IS EMPTY! (clicked 2nd column?)";

        return;
    }

    QTextCursor textCursor = this->textCursor();
    textCursor.select(QTextCursor::WordUnderCursor);
    textCursor.removeSelectedText();

    this->insertHtml("<a href=\"" + nickUrl + "\">"
                     + nickName + "</a>");
    this->makeNormal();

    // Send signal for Publisher(), to add to the "To" field
    emit nickInserted(nickId, nickName, nickUrl);
}




/*
 * Cancel editing of the post, clear it, return to minimum mode
 *
 */
void Composer::cancelPost()
{
    int cancelConfirmed = 1;

    if (this->document()->isEmpty())
    {
        cancelConfirmed = 0; // Cancelling doesn't need confirmation if it's empty
    }
    else
    {
        cancelConfirmed = QMessageBox::question(this,
                                                tr("Cancel message?"),
                                                tr("Are you sure you want to "
                                                   "cancel this message?"),
                                                tr("&Yes, cancel it"),
                                                tr("&No"),
                                                "", 1, 1);

    }


    if (cancelConfirmed == 0)
    {
        this->erase();

        // emit signal to make Publisher go back to minimum mode
        emit editingCancelled();

        qDebug() << "Post cancelled";
    }
}
