/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef HCLABEL_H
#define HCLABEL_H

#include <QLabel>
#include <QMouseEvent>
#include <QTimer>

#include <QDebug>


class HClabel : public QLabel
{
    Q_OBJECT

public:
    explicit HClabel(QString initialText = QString(),
                     QWidget *parent = 0);
    ~HClabel();

    void setHighlighted(bool highlighted);

    void setBaseText(QString text);
    void setExtendedText(QString text);

    void setExpanded(bool isExpanded);


signals:
    void clicked();


public slots:
    void toggleContent();


protected:
    virtual void mousePressEvent(QMouseEvent *event);


private:
    QString baseText;
    QString extendedText;

    bool expanded;

    QTimer *toggleTimer;
};

#endif // HCLABEL_H
