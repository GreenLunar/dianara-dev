/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef EMAILCHANGER_H
#define EMAILCHANGER_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QAction>
#include <QCloseEvent>

#include <QDebug>

#include "pumpcontroller.h"


class EmailChanger : public QWidget
{
    Q_OBJECT

public:
    explicit EmailChanger(QString explanation,
                          PumpController *pumpController,
                          QWidget *parent = 0);
    ~EmailChanger();

    void setCurrentEmail(QString email);


signals:


public slots:
    void validateFields();
    void changeEmail();
    void cancelDialog();


protected:
    virtual void closeEvent(QCloseEvent *event);


private:
    QVBoxLayout *mainLayout;
    QFormLayout *middleLayout;
    QHBoxLayout *bottomLayout;

    QLabel *infoLabel;

    QLineEdit *mailLineEdit;
    QLineEdit *mailRepeatLineEdit;
    QLineEdit *passwordLineEdit;

    QLabel *errorsLabel;

    QPushButton *changeButton;
    QPushButton *cancelButton;

    QAction *cancelAction;

    QString currentEmail;

    PumpController *pController;
};

#endif // EMAILCHANGER_H
