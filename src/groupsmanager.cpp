/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "groupsmanager.h"

GroupsManager::GroupsManager(PumpController *pumpController,
                             QWidget *parent) : QWidget(parent)
{
    this->pController = pumpController;

    this->setWindowTitle("GROUPS MANAGER"  " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("user-group-properties"));
    this->setWindowFlags(Qt::Dialog);
    this->setWindowModality(Qt::ApplicationModal);
    this->setMinimumSize(400, 400);

    this->newGroupNameLineEdit = new QLineEdit();
    newGroupNameLineEdit->setPlaceholderText("Name for the new group");

    this->newGroupSummaryLineEdit = new QLineEdit();
    newGroupSummaryLineEdit->setPlaceholderText("Summary");

    this->newGroupDescLineEdit = new QLineEdit();
    newGroupDescLineEdit->setPlaceholderText("Longer description of the group");

    this->createGroupButton = new QPushButton(QIcon::fromTheme("user-group-new"),
                                              "&CREATE GROUP");
    connect(createGroupButton, SIGNAL(clicked()),
            this, SLOT(createGroup()));

    this->joinLeaveGroupLineEdit = new QLineEdit();
    joinLeaveGroupLineEdit->setPlaceholderText("ID of the group you wish to join or leave");

    this->joinGroupButton = new QPushButton(QIcon::fromTheme("list-add-user"),
                                            "&JOIN!");
    connect(joinGroupButton, SIGNAL(clicked()),
            this, SLOT(joinGroup()));

    this->leaveGroupButton = new QPushButton(QIcon::fromTheme("list-remove-user"),
                                             "&LEAVE");
    connect(leaveGroupButton, SIGNAL(clicked()),
            this, SLOT(leaveGroup()));



    closeAction = new QAction(this);
    closeAction->setShortcut(QKeySequence(Qt::Key_Escape));
    connect(closeAction, SIGNAL(triggered()),
            this, SLOT(hide()));
    this->addAction(closeAction);

    // Layout
    this->mainLayout = new QVBoxLayout();
    mainLayout->setAlignment(Qt::AlignTop);
    mainLayout->addWidget(newGroupNameLineEdit);
    mainLayout->addWidget(newGroupSummaryLineEdit);
    mainLayout->addWidget(newGroupDescLineEdit);
    mainLayout->addWidget(createGroupButton);
    mainLayout->addStretch(1);
    mainLayout->addWidget(joinLeaveGroupLineEdit);
    mainLayout->addWidget(joinGroupButton);
    mainLayout->addWidget(leaveGroupButton);
    this->setLayout(mainLayout);

    qDebug() << "GroupsManager created";

}

GroupsManager::~GroupsManager()
{
    qDebug() << "GroupsManager destroyed";
}


////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// SLOTS /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void GroupsManager::createGroup()
{
    if (newGroupNameLineEdit->text().trimmed().isEmpty())
    {
        QMessageBox::warning(this, "ERROR",
                             "Groups require a name.");
        return;
    }
    qDebug() << "Creating group...";

    this->pController->createGroup(this->newGroupNameLineEdit->text().trimmed(),
                                   this->newGroupSummaryLineEdit->text().trimmed(),
                                   this->newGroupDescLineEdit->text().trimmed());

    this->newGroupNameLineEdit->clear();
    this->newGroupSummaryLineEdit->clear();
    this->newGroupDescLineEdit->clear();
}


void GroupsManager::deleteGroup()
{

}


void GroupsManager::joinGroup()
{
    if (joinLeaveGroupLineEdit->text().trimmed().isEmpty())
    {
        QMessageBox::warning(this, "ERROR",
                             "ID of group to join is empty.");
        return;
    }

    qDebug() << "Joining group...";
    this->pController->joinGroup(this->joinLeaveGroupLineEdit->text().trimmed());

    this->joinLeaveGroupLineEdit->clear();
}


void GroupsManager::leaveGroup()
{
    if (joinLeaveGroupLineEdit->text().trimmed().isEmpty())
    {
        QMessageBox::warning(this, "ERROR",
                             "ID of group to leave is empty.");
        return;
    }
    qDebug() << "Leaving group...";
    this->pController->leaveGroup(this->joinLeaveGroupLineEdit->text().trimmed());

    this->joinLeaveGroupLineEdit->clear();
}
