/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "asobject.h"

ASObject::ASObject(QVariantMap objectMap,
                   QObject *parent) : QObject(parent)
{
    this->originalObjectMap = objectMap;

    /// Meta information
    id = ASPerson::cleanupId(objectMap.value("id").toString());

    type = objectMap.value("objectType").toString();
    url = objectMap.value("url").toString();
    createdAt = objectMap.value("published").toString();
    updatedAt = objectMap.value("updated").toString();

    // FIXME: still missing some fields...
    QVariantMap locationMap = objectMap.value("location").toMap();
    locationName = locationMap.value("displayName").toString();
    locationFormatted = locationMap.value("address").toMap().value("formatted").toString();
    locationCountry = locationMap.value("address").toMap().value("country").toString();


    // Author
    this->asAuthor = new ASPerson(objectMap.value("author").toMap(), this);

    // This will hold the date when the object was deleted, or empty if it wasn't
    deleted = objectMap.value("deleted").toString();

    liked = objectMap.value("liked").toString();

    // The object to which this one replies, if any (note to which a comment replies, etc)
    inReplyToMap = objectMap.value("inReplyTo").toMap();


    ///
    /// End of "meta"; Start of content
    ///


    if (type == "image")
    {
        /* Get the "small" version.
         * See if there's a proxy URL for the image first
         * (for private images on remote servers)
         */
        qDebug() << "Trying Proxyed thumbnail image";
        smallImageUrl = objectMap.value("image").toMap()
                                 .value("pump_io").toMap()
                                 .value("proxyURL").toString();

        // And if that's empty, use regular image->url
        if (smallImageUrl.isEmpty())
        {
            qDebug() << "Trying direct thumbnail image";
            smallImageUrl = objectMap.value("image").toMap()
                                     .value("url").toString();
        }


        // Same for the full size image
        imageUrl = objectMap.value("fullImage").toMap()
                            .value("pump_io").toMap()
                            .value("proxyURL").toString();
        qDebug() << "Trying Proxyed fullImage";

        // If that's empty, use regular fullImage->url field
        if (imageUrl.isEmpty())
        {
            qDebug() << "Trying direct fullImage";
            imageUrl = objectMap.value("fullImage").toMap()
                                .value("url").toString();
        }

        imageWidth = objectMap.value("fullImage").toMap()
                              .value("width").toInt();
        imageHeight = objectMap.value("fullImage").toMap()
                               .value("height").toInt();

        // If it's STILL empty, use the thumbnail also as full image
        if (imageUrl.isEmpty())
        {
            imageUrl = smallImageUrl;
            imageWidth = objectMap.value("image").toMap()
                                  .value("width").toInt();
            imageHeight = objectMap.value("image").toMap()
                                   .value("height").toInt();
        }        

        qDebug() << "postImage:" << imageUrl << "> Small:" << smallImageUrl;

        // To have a filename with extension, even when downloading from proxyURL's
        attachmentPureUrl = objectMap.value("fullImage").toMap()
                                     .value("url").toString();
        if (attachmentPureUrl.isEmpty())
        {
            attachmentPureUrl = objectMap.value("image").toMap()
                                         .value("url").toString();
        }
    }


    // Get audio file URL
    if (type == "audio")
    {
        // To have a filename with extension, even when downloading from proxyURL's
        attachmentPureUrl = objectMap.value("stream").toMap().value("url").toString();

        audioUrl = objectMap.value("stream").toMap()
                            .value("pump_io").toMap()
                            .value("proxyURL").toString();
        if (audioUrl.isEmpty())
        {
            qDebug() << "No proxyed link to audio, fetching regular link";
            audioUrl = attachmentPureUrl;
        }
    }

    // Get video file URL
    if (type == "video")
    {
        // To have a filename with extension, even when downloading from proxyURL's
        attachmentPureUrl = objectMap.value("stream").toMap().value("url").toString();

        videoUrl = objectMap.value("stream").toMap()
                            .value("pump_io").toMap()
                            .value("proxyURL").toString();
        if (videoUrl.isEmpty())
        {
            qDebug() << "No proxyed link to video, fetching regular link";
            videoUrl = attachmentPureUrl;
        }
    }

    // Get general file URL
    if (type == "file")
    {
        // To have a filename with extension, even when downloading from proxyURL's
        attachmentPureUrl = objectMap.value("fileUrl").toString();

        // FIXME: there are no proxyURL for misc files ATM; not my fault though =)
        fileUrl = objectMap.value("fileUrl").toMap()
                           .value("pump_io").toMap()
                           .value("proxyURL").toString();
        if (fileUrl.isEmpty())
        {
            qDebug() << "No proxyed link to general file, fetching regular link";
            fileUrl = attachmentPureUrl;
        }

        mimeType = objectMap.value("mimeType").toString();
        qDebug() << "file mimeType:" << mimeType;
    }


    // If it's a group, get member count, etc.
    if (type == "group")
    {
        memberCount = objectMap.value("members").toMap()
                               .value("totalItems").toInt();
        memberUrl = objectMap.value("members").toMap()
                             .value("url").toString();
    }



    // Title can be in non-image posts, too!
    title = objectMap.value("displayName").toString().trimmed();
    summary = objectMap.value("summary").toString();
    content = objectMap.value("content").toString();

    likesCount = objectMap.value("likes").toMap().value("totalItems").toString();
    if (likesCount.isEmpty())
    {
        likesCount = "0";
    }
    commentsCount = objectMap.value("replies").toMap().value("totalItems").toString();
    sharesCount = objectMap.value("shares").toMap().value("totalItems").toString();

    // Get last likes, comments and shares list here, used from Post()
    lastLikesList = objectMap.value("likes").toMap().value("items").toList();
    lastCommentsList = objectMap.value("replies").toMap().value("items").toList();
    lastSharesList = objectMap.value("shares").toMap().value("items").toList();

    proxiedUrls = false;

    // Get URL for likes; first, proxyURL if it exists
    likesUrl = objectMap.value("likes").toMap().value("pump_io").toMap()
                                               .value("proxyURL").toString();
    // If still empty, get regular URL (that means the post is in the same server we are)
    if (likesUrl.isEmpty())
    {
        likesUrl = objectMap.value("likes").toMap().value("url").toString();
    }

    // Get URL for comments; first, proxyURL if it exists
    commentsUrl = objectMap.value("replies").toMap().value("pump_io").toMap()
                                                    .value("proxyURL").toString();
    if (commentsUrl.isEmpty()) // If still empty, get regular URL
    {
        commentsUrl = objectMap.value("replies").toMap().value("url").toString();
    }
    else
    {
        this->proxiedUrls = true;
    }

    // FIXME: get sharesUrl...

    qDebug() << "ASObject created" << this->id;
}

ASObject::~ASObject()
{
    qDebug() << "ASObject destroyed" << this->id;
}



void ASObject::updateAuthorFromPerson(ASPerson *person)
{
    this->asAuthor->updateDataFromPerson(person);
}



/////// Getters

ASPerson *ASObject::author()
{
    return this->asAuthor;
}

QString ASObject::getId()
{
    return this->id;
}

QString ASObject::getType()
{
    return this->type;
}

QString ASObject::getTranslatedType(QString typeString)
{
    QString translatedTypeString;

    if (typeString == "note")
    {
        translatedTypeString = tr("Note",
                                  "Noun, an object type");
    }
    else if (typeString == "article")
    {
        translatedTypeString = tr("Article",
                                  "Noun, an object type");
    }
    else if (typeString == "image")
    {
        translatedTypeString = tr("Image",
                                  "Noun, an object type");
    }
    else if (typeString == "audio")
    {
        translatedTypeString = tr("Audio",
                                  "Noun, an object type");
    }
    else if (typeString == "video")
    {
        translatedTypeString = tr("Video",
                                  "Noun, an object type");
    }
    else if (typeString == "file")
    {
        translatedTypeString = tr("File",
                                  "Noun, an object type");
    }
    else if (typeString == "comment")
    {
        translatedTypeString = tr("Comment",
                                  "Noun, as in object type: a comment");
    }
    else if (typeString == "group")
    {
        translatedTypeString = tr("Group",
                                  "Noun, an object type");
    }
    else if (typeString == "collection")
    {
        translatedTypeString = tr("Collection",
                                  "Noun, an object type");
    }
    else
    {
        translatedTypeString = tr("Other",
                                  "As in: other type of post")
                               + " (" + typeString + ")";
    }

    return translatedTypeString;
}


QString ASObject::getUrl()
{
    return this->url;
}

QString ASObject::getCreatedAt()
{
    return this->createdAt;
}

QString ASObject::getUpdatedAt()
{
    return this->updatedAt;
}

QString ASObject::getLocationName()
{
    return this->locationName;
}

QString ASObject::getLocationFormatted()
{
    return this->locationFormatted;
}

QString ASObject::getLocationCountry()
{
    return this->locationCountry;
}

QString ASObject::getLocationTooltip()
{
    QString locationTooltip = this->locationFormatted;

    if (!this->locationCountry.isEmpty())
    {
        if (!locationTooltip.isEmpty())
        {
            locationTooltip.append("\n");
        }

        locationTooltip.append("(" + this->locationCountry +  ")");
    }

    // If still empty, return informational string
    if (locationTooltip.isEmpty())
    {
        locationTooltip = tr("No detailed location");
    }

    return locationTooltip;
}


QString ASObject::getDeletedTime()
{
    return this->deleted;
}

QString ASObject::getDeletedOnString()
{
    QString deletedOnString;
    if (!this->deleted.isEmpty())
    {
        deletedOnString = this->makeDeletedOnString(this->deleted);
    }

    return deletedOnString;
}

QString ASObject::makeDeletedOnString(QString deletionTime)
{
    return tr("Deleted on %1").arg(Timestamp::localTimeDate(deletionTime));
}


QString ASObject::isLiked()
{
    return this->liked;
}


QString ASObject::getTitle()
{
    return this->title;
}

QString ASObject::getSummary()
{
    return this->summary;
}

QString ASObject::getContent()
{
    return this->content;
}

QString ASObject::getImageUrl()
{
    return this->imageUrl;
}

QString ASObject::getSmallImageUrl()
{
    return this->smallImageUrl;
}

int ASObject::getImageWidth()
{
    return this->imageWidth;
}

int ASObject::getImageHeight()
{
    return this->imageHeight;
}

QString ASObject::getAudioUrl()
{
    return this->audioUrl;
}

QString ASObject::getVideoUrl()
{
    return this->videoUrl;
}

QString ASObject::getFileUrl()
{
    return this->fileUrl;
}

QString ASObject::getMimeType()
{
    return this->mimeType;
}

QString ASObject::getAttachmentPureUrl()
{
    return this->attachmentPureUrl;
}

int ASObject::getMemberCount()
{
    return this->memberCount;
}

QString ASObject::getMemberUrl()
{
    return this->memberUrl;
}

QString ASObject::getLikesCount()
{
    return this->likesCount;
}

QString ASObject::getCommentsCount()
{
    return this->commentsCount;
}

QString ASObject::getSharesCount()
{
    return this->sharesCount;
}

bool ASObject::hasProxiedUrls()
{
    return this->proxiedUrls;
}

QVariantList ASObject::getLastLikesList()
{
    return this->lastLikesList;
}

QVariantList ASObject::getLastCommentsList()
{
    return this->lastCommentsList;
}

QVariantList ASObject::getLastSharesList()
{
    return this->lastSharesList;
}

QString ASObject::getLikesUrl()
{
    return this->likesUrl;
}

QString ASObject::getCommentsUrl()
{
    return this->commentsUrl;
}

QString ASObject::getSharesUrl()
{
    return this->sharesUrl;
}

QVariantMap ASObject::getOriginalObject()
{
    return this->originalObjectMap;
}

QVariantMap ASObject::getInReplyTo()
{
    return this->inReplyToMap;
}

QString ASObject::getInReplyToId()
{
    return inReplyToMap.value("id").toString();
}


/*
 * This version should be deprecated soon -- WIP
 *
 */
QString ASObject::personStringFromList(QVariantList variantList, int count)
{
    QString personString;

    foreach (QVariant listItem, variantList)
    {
        QVariantMap itemMap = listItem.toMap();

        QString name = itemMap.value("displayName").toString();
        if (name.isEmpty())
        {
            name = ASPerson::cleanupId(itemMap.value("id").toString()); // fallback
        }
        QString profileUrl = itemMap.value("url").toString();

        personString.prepend("<a href=\"" + profileUrl + "\">"
                             + name + "</a>, "); // Reverse order
    }

    int remainingCount = count - variantList.size();

    // Add "and $COUNT other"
    if (count != -1 && remainingCount > 0)
    {
        if (remainingCount == 1)
        {
            personString.append(tr("and one other"));
        }
        else
        {
            personString.append(tr("and %1 others").arg(remainingCount));
        }
    }
    else
    {
        // Remove last comma and space
        personString.remove(-2, 2);
    }


    return personString;
} // deprecating... WIP



QVariantMap ASObject::simplePersonMapFromList(QVariantList variantList)
{
    QVariantMap map;

    // Read the list in reverse order:

    for (int position = variantList.size() - 1; position >= 0; --position)
    {
        QVariantMap itemMap = variantList.at(position).toMap();

        QString id = ASPerson::cleanupId(itemMap.value("id").toString());
        QString name = itemMap.value("displayName").toString();
        if (name.isEmpty())
        {
            name = id; // fallback
        }
        QString profileUrl = itemMap.value("url").toString();

        ASObject::addOnePersonToSimpleMap(id, name, profileUrl,
                                          &map);
    }

    return map;
}


void ASObject::addOnePersonToSimpleMap(QString personId, QString personName,
                                       QString personUrl, QVariantMap *personMap)
{
    personMap->insert(personId, "<a href=\"" + personUrl + "\">"
                                + personName + "</a>");
}


QString ASObject::personStringFromSimpleMap(QVariantMap personMap,
                                            int totalCount)
{
    QString personString;

    foreach (QString key, personMap.keys())
    {
        personString.append(personMap.value(key).toString() + ", ");
    }

    int remainingCount = totalCount - personMap.keys().length();

    // Add "and $COUNT other"
    if (remainingCount > 0)
    {
        if (remainingCount == 1)
        {
            personString.append(tr("and one other"));
        }
        else
        {
            personString.append(tr("and %1 others").arg(remainingCount));
        }
    }
    else
    {
        // Remove last comma and space
        personString.remove(-2, 2);
    }

    return personString;
}



/*
 * Returns true if objectType is one of the types that Post() can display
 *
 *
 */
bool ASObject::canDisplayObject(QString objectType)
{
    bool canDisplay = false;

    if (objectType == "note"
     || objectType == "article"
     || objectType == "image"
     || objectType == "audio"
     || objectType == "video"
     || objectType == "file"
     || objectType == "comment"
     || objectType == "group")
    {
        canDisplay = true;
    }
    else
    {
        qDebug() << "ASObject::canDisplayObject() - Unsupported:"
                 << objectType;
    }

    return canDisplay;
}
