/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QWidget>
#include <QSettings>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QByteArray>
#include <QHideEvent>

#include <QDebug>

#include "pumpcontroller.h"


class AccountDialog : public QWidget
{
    Q_OBJECT

public:
    AccountDialog(PumpController *pumpController, QWidget *parent = 0);
    ~AccountDialog();
    void setLockMode(bool locked);

signals:
    void userIDChanged(QString newUserID);

public slots:
    void askForToken();
    void setVerifierCode();
    void showAuthorizationStatus(bool authorized);
    void showAuthorizationURL(QUrl url);

    void saveDetails();

    void unlockDialog();

protected:
    virtual void hideEvent(QHideEvent *event);

private:
    QVBoxLayout *mainLayout;

    QLabel *helpMessage1Label;

    QHBoxLayout *idLayout;
    QLabel *userIDIconLabel;
    QLabel *userIdLabel;
    QLineEdit *userIdLineEdit;

    QPushButton *getVerifierButton;

    QFrame *separatorLine;

    QLabel *helpMessage2Label;

    QHBoxLayout *verifierLayout;
    QLabel *verifierIconLabel;
    QLabel *verifierLabel;
    QLineEdit *verifierLineEdit;

    QPushButton *authorizeApplicationButton;


    QLabel *errorsLabel;
    QLabel *authorizationStatusLabel;

    QLabel *unlockExplanationLabel;
    QPushButton *unlockButton;

    QHBoxLayout *buttonsLayout;
    QPushButton *saveButton;
    QPushButton *cancelButton;


    PumpController *pController;
};

#endif // ACCOUNT_H
