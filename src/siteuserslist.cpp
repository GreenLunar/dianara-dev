/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "siteuserslist.h"


SiteUsersList::SiteUsersList(PumpController *pumpController,
                             GlobalObject *globalObject,
                             QWidget *parent) : QWidget(parent)
{
    this->pController = pumpController;

    this->explanationLabel = new QLabel("<br><br>"
                       + tr("You can get a list of the newest users registered "
                            "on your server by clicking the button below.")
                       + "<br><br><hr><br><br>"
                       + tr("More resources to find users:")
                       + "<ul>"
                         "<li>"
                         "<a href=\"https://github.com/e14n/pump.io/wiki/Users-by-language\">"
                       + tr("Wiki page 'Users by language'")
                       + "</a>."
                         "</li>"
                         "<li>"
                         "<a href=\"https://www.inventati.org/ppump/usuarios/\">"
                       + tr("PPump user search service at inventati.org")
                       + "</a>."
                         "</li>"
                         "<li>"
                         "<a href=\"https://pumpit.info/pumpio/followers\">"
                       + tr("List of Followers for the Pump.io Community account")
                       + "</a>."
                         "</li>"
                         "</ul>"
                         "<br><br>", this);
    explanationLabel->setWordWrap(true);
    explanationLabel->setAlignment(Qt::AlignTop);
    explanationLabel->setMargin(8);
    explanationLabel->setOpenExternalLinks(true);

    this->getListButton = new QPushButton(QIcon::fromTheme("system-users",
                                                           QIcon(":/images/no-avatar.png")),
                                          tr("Get list of users from your server"),
                                          this);
    connect(getListButton, SIGNAL(clicked()),
            this, SLOT(getList()));


    this->serverInfoLabel = new QLabel(this);
    serverInfoLabel->setWordWrap(true);
    serverInfoLabel->setAlignment(Qt::AlignCenter);
    serverInfoLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
    serverInfoLabel->setMargin(4);
    serverInfoLabel->hide();

    this->closeListButton = new QPushButton(QIcon::fromTheme("window-close",
                                                             QIcon(":/images/button-close.png")),
                                            "",
                                            this);
    closeListButton->setSizePolicy(QSizePolicy::Maximum,
                                   QSizePolicy::Maximum);
    closeListButton->setToolTip(tr("Close list"));
    connect(closeListButton, SIGNAL(clicked()),
            this, SLOT(closeList()));
    closeListButton->hide();


    this->userList = new ContactList(this->pController,
                                     globalObject,
                                     "site-users",
                                     this);
    this->userList->hide();
    connect(pController, SIGNAL(siteUserListReceived(QVariantList,int)),
            this, SLOT(setListContents(QVariantList,int)));


    this->middleLayout = new QHBoxLayout();
    middleLayout->addWidget(serverInfoLabel);
    middleLayout->addWidget(closeListButton);


    this->mainLayout = new QVBoxLayout();
    this->mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(explanationLabel);
    mainLayout->addWidget(getListButton, 1, Qt::AlignTop | Qt::AlignHCenter);
    mainLayout->addLayout(middleLayout);
    mainLayout->addWidget(userList);
    this->setLayout(mainLayout);

    qDebug() << "SiteUsersList created";
}

SiteUsersList::~SiteUsersList()
{
    qDebug() << "SiteUsersList destroyed";
}


/*****************************************************************************/
/*********************************** SLOTS ***********************************/
/*****************************************************************************/


void SiteUsersList::getList()
{
    this->pController->getSiteUserList();

    this->explanationLabel->hide();
    this->getListButton->hide();


    this->serverInfoLabel->setText(tr("Loading..."));
    this->serverInfoLabel->show();
    this->closeListButton->show();

    this->userList->clearListContents();
    this->userList->show();
}


void SiteUsersList::setListContents(QVariantList userList, int totalUsers)
{
    this->serverInfoLabel->setText(tr("%1 users in %2",
                                      "%1 = user count, %2 = server name")
                                   .arg(QLocale::system().toString(totalUsers))
                                   .arg(this->pController->currentServerUrl()));

    this->userList->setListContents(userList);
}


void SiteUsersList::closeList()
{
    this->explanationLabel->show();
    this->getListButton->show();

    this->serverInfoLabel->hide();
    this->closeListButton->hide();

    this->userList->clearListContents();
    this->userList->hide();
}

