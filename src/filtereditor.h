/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef FILTEREDITOR_H
#define FILTEREDITOR_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QIcon>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QListWidget>
#include <QVariantList>
#include <QVariantMap>
#include <QSettings>
#include <QAction>

#include <QDebug>

#include "filterchecker.h"

class FilterEditor : public QWidget
{
    Q_OBJECT

public:
    explicit FilterEditor(FilterChecker *filterChecker,
                          QWidget *parent = 0);
    ~FilterEditor();

    void loadFilters();

signals:

public slots:
    void onFilterTextChanged(QString text);
    void onFilterRowChanged(int row);

    void addFilter();
    void removeFilter();

    void saveFilters();

private:
    QVBoxLayout *mainLayout;
    QHBoxLayout *topLayout;
    QGroupBox *newFilterGroupBox;
    QVBoxLayout *middleLayout;
    QGroupBox *currentFiltersGroupBox;
    QHBoxLayout *bottomLayout;


    QAction *closeAction;

    QLabel *explanationLabel;

    QComboBox *actionTypeComboBox;
    QComboBox *filterTypeComboBox;
    QLineEdit *filterWordsLineEdit;
    QPushButton *addFilterButton;

    QListWidget *currentFiltersListWidget;
    QPushButton *removeFilterButton;

    QPushButton *saveButton;
    QPushButton *cancelButton;

    QString ruleString;
    QVariantList filtersList;

    FilterChecker *fChecker;
};

#endif // FILTEREDITOR_H
