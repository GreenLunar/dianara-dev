/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "bannernotification.h"


BannerNotification::BannerNotification(QWidget *parent) : QWidget(parent)
{
    /*
     * In the future, this widget might be used to notify different things.
     *
     * For now, it's only used to passively notify about pending autoupdates.
     *
     */

    this->iconLabel = new QLabel(this);
    iconLabel->setPixmap(QIcon::fromTheme("dialog-information",    // "clock", maybe?
                                          QIcon(":/images/feed-clock.png")) // FIXME: proper fallback
                         .pixmap(32, 32));

    this->descriptionLabel = new QLabel(tr("Timelines were not automatically "
                                           "updated to avoid interruptions."),
                                        this);
    descriptionLabel->setAlignment(Qt::AlignCenter);
    descriptionLabel->setWordWrap(true);
    // CSS for text color only; container widget has CSS for the background
    descriptionLabel->setStyleSheet("QLabel"
                                    "{ color: palette(highlighted-text); }");
    descriptionLabel->setToolTip("<b></b>"
                                 + tr("This happens when it is time to "
                                      "autoupdate the timelines, but you are "
                                      "not at the top of the first page, to "
                                      "avoid interruptions while you read"));


    this->okButton = new QPushButton(QIcon::fromTheme("view-refresh",
                                                      QIcon(":/images/menu-refresh.png")),
                                     tr("Update now"),
                                     this);
    connect(okButton, SIGNAL(clicked(bool)),
            this, SLOT(onOk()));

    this->cancelButton = new QPushButton(QIcon::fromTheme("window-close",
                                                          QIcon(":/images/button-close.png")),
                                         "",
                                         this);
    cancelButton->setToolTip("<b></b>"
                             + tr("Hide this message"));
    connect(cancelButton, SIGNAL(clicked(bool)),
            this, SLOT(onCancel()));


    this->containerLayout = new QHBoxLayout();
    containerLayout->addWidget(iconLabel);
    containerLayout->addWidget(descriptionLabel, 1);
    containerLayout->addWidget(okButton);
    containerLayout->addWidget(cancelButton);


    this->containerWidget = new QWidget();
    containerWidget->setObjectName("BannerWidget");
    containerWidget->setStyleSheet("QWidget#BannerWidget                    "
                                   "{ background-color: palette(highlight); "
                                   "  color: palette(highlighted-text);     "
                                   "  padding: 2px;                         "
                                 //"  border: 1px solid red;                "
                                 //"          palette(highlighted-text);    "
                                   "  border-radius: 12px                   "
                                   "}");
    containerWidget->setLayout(containerLayout);


    this->mainLayout = new QHBoxLayout();
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(containerWidget);
    this->setLayout(mainLayout);

    qDebug() << "BannerNotification created";
}


BannerNotification::~BannerNotification()
{
    qDebug() << "BannerNotification destroyed";
}


/*****************************************************************************/
/*********************************** SLOTS ***********************************/
/*****************************************************************************/


void BannerNotification::onOk()
{
    this->hide();

    emit updateRequested();
}


void BannerNotification::onCancel()
{
    this->hide();

    emit bannerCancelled();
}

