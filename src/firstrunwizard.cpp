/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "firstrunwizard.h"

FirstRunWizard::FirstRunWizard(AccountDialog *accountDlg,
                               ProfileEditor *profileEd,
                               ConfigDialog *configDlg,
                               HelpWidget *helpWdg,
                               GlobalObject *globalObj,
                               QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Welcome Wizard") + " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("tools-wizard"));
    this->setWindowFlags(Qt::Window);
    this->setMinimumSize(420, 460);
    this->resize(520, 620);

    this->accountDialog = accountDlg;
    this->profileEditor = profileEd;
    this->configDialog = configDlg;
    this->helpWidget = helpWdg;
    this->globalObject = globalObj;

    this->explanationLabel = new QLabel("<big><b>" + tr("Welcome to Dianara!")
                                        + "</b></big>"
                                        + "<br><br>"
                                        + tr("This wizard will help you "
                                             "get started.")
                                        + " "
                                        + tr("You can access this window again "
                                             "at any time from the Help menu.")
                                        + "<br><br>"
                                        + tr("The first step is setting up "
                                             "your account, by using the "
                                             "following button:"),
                                        this);
    explanationLabel->setWordWrap(true);
    explanationLabel->setAlignment(Qt::AlignTop);
    explanationLabel->setOpenExternalLinks(true);


    // Config account
    this->configureAccountButton = new QPushButton(QIcon::fromTheme("dialog-password",
                                                                    QIcon(":/images/button-password.png")),
                                                   tr("Configure your &account"),
                                                   this);
    connect(configureAccountButton, SIGNAL(clicked()),
            accountDialog, SLOT(show()));



    // Edit profile
    this->editProfileLabel = new QLabel(tr("Once you have configured your "
                                           "account, it's recommended that you "
                                           "edit your profile and add an avatar "
                                           "and some other information, if you "
                                           "haven't done so already."),
                                        this);
    editProfileLabel->setWordWrap(true);
    editProfileLabel->setAlignment(Qt::AlignTop);

    this->editProfileButton = new QPushButton(QIcon::fromTheme("user-properties",
                                                               QIcon(":/images/no-avatar.png")),
                                              tr("&Edit your profile"),
                                              this);
    //this->editProfileButton->setDisabled(true); // TMP FIXME
    connect(editProfileButton, SIGNAL(clicked()),
            profileEditor, SLOT(show()));



    // Public posting
    this->publicPostsLabel = new QLabel(tr("By default, Dianara will post only "
                                           "to your followers, but it's "
                                           "recommended that you post to "
                                           "Public, at least sometimes."),
                                        this);
    publicPostsLabel->setWordWrap(true);
    publicPostsLabel->setAlignment(Qt::AlignTop);

    this->publicPostsCheckbox = new QCheckBox(tr("Post to &Public by default"),
                                              this);
    publicPostsCheckbox->setChecked(globalObject->getPublicPostsByDefault());



    // Access to program help
    this->helpButton = new QPushButton(QIcon::fromTheme("system-help",
                                                        QIcon(":/images/menu-find.png")),
                                       tr("Open general program &help window"),
                                       this);
    connect(helpButton, SIGNAL(clicked()),
            helpWidget, SLOT(show()));


    // Bottom

    this->showAgainCheckbox = new QCheckBox(tr("&Show this again next time "
                                               "Dianara starts"),
                                            this);

    this->closeButton = new QPushButton(QIcon::fromTheme("window-close",
                                                         QIcon(":/images/button-close.png")),
                                        tr("&Close"),
                                        this);
    connect(closeButton, SIGNAL(clicked()),
            this, SLOT(close()));


    // Layout
    this->bottomLayout = new QHBoxLayout();
    this->bottomLayout->addWidget(showAgainCheckbox);
    this->bottomLayout->addWidget(closeButton, 0, Qt::AlignRight);

    this->mainLayout = new QVBoxLayout();
    mainLayout->addWidget(explanationLabel);
    mainLayout->addSpacing(12);
    mainLayout->addWidget(configureAccountButton, 0, Qt::AlignCenter);
    mainLayout->addStretch(1);
    mainLayout->addWidget(editProfileLabel);
    mainLayout->addSpacing(12);
    mainLayout->addWidget(editProfileButton,      0, Qt::AlignCenter);
    mainLayout->addStretch(1);
    mainLayout->addWidget(publicPostsLabel);
    mainLayout->addSpacing(12);
    mainLayout->addWidget(publicPostsCheckbox,    0, Qt::AlignCenter);
    mainLayout->addStretch(1);
    mainLayout->addSpacing(16);
    mainLayout->addWidget(helpButton,             0, Qt::AlignCenter);
    mainLayout->addStretch(1);
    mainLayout->addSpacing(16);
    mainLayout->addLayout(bottomLayout);
    this->setLayout(mainLayout);


    QSettings settings;
    showAgainCheckbox->setChecked(settings.value("FirstRunWizard/showWizard",
                                                 true).toBool());

    qDebug() << "FirstRunWizard created";
}

FirstRunWizard::~FirstRunWizard()
{
    qDebug() << "FirstRunWizard destroyed";
}


/*****************************************************************************/
/*********************************** SLOTS ***********************************/
/*****************************************************************************/



/*****************************************************************************/
/********************************* PROTECTED *********************************/
/*****************************************************************************/


void FirstRunWizard::closeEvent(QCloseEvent *event)
{
    QSettings settings;
    settings.setValue("FirstRunWizard/showWizard",
                      this->showAgainCheckbox->isChecked());
    settings.sync();

    // Sync public posting option
    this->configDialog->setPublicPosts(this->publicPostsCheckbox->isChecked());


    this->hide(); // close() would kill the program if mainWindow was hidden
    this->deleteLater();
    event->ignore();
}

