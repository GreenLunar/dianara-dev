/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef AUDIENCESELECTOR_H
#define AUDIENCESELECTOR_H

#include <QFrame>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QGroupBox>
#include <QListWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QAction>
#include <QCloseEvent>
#include <QHideEvent>
#include <QSettings>

#include <QDebug>

#include "pumpcontroller.h"
#include "peoplewidget.h"


class AudienceSelector : public QFrame
{
    Q_OBJECT

public:
    explicit AudienceSelector(PumpController *pumpController,
                              QString selectorType,
                              QWidget *parent = 0);
    ~AudienceSelector();

    void resetLists();
    void deletePrevious();
    void saveSelected();
    void restoreSelected();


signals:
    void audienceSelected(QString selectorType,
                          QStringList contactsList,
                          QStringList urlsList);


public slots:
    void copyToSelected(QIcon contactIcon, QString contactString,
                        QString contactUrl);
    void setAudience();


protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void hideEvent(QHideEvent *event);


private:
    QString selectorType;

    QHBoxLayout *mainLayout;


    QVBoxLayout *allGroupboxLayout;
    QGroupBox *allContactsGroupbox;

    PeopleWidget *peopleWidget;

    QVBoxLayout *selectedGroupboxLayout;
    QGroupBox *selectedListGroupbox;

    QLabel *explanationLabel;
    QListWidget *selectedListWidget;

    QList<QListWidgetItem *> previousItems;

    QPushButton *clearSelectedListButton;

    QHBoxLayout *buttonsLayout;
    QPushButton *doneButton;
    QPushButton *cancelButton;

    QAction *doneAction;
    QAction *cancelAction;
};

#endif // AUDIENCESELECTOR_H
