/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "filterchecker.h"


FilterChecker::FilterChecker(QObject *parent) : QObject(parent)
{
    qDebug() << "FilterChecker created";
}

FilterChecker::~FilterChecker()
{
    qDebug() << "FilterChecker destroyed";
}


/*
 * Set or update filter rules; called from the FilterEditor
 *
 */
void FilterChecker::setFilters(QVariantList newFiltersList)
{
    filteredContent.clear();
    filteredAuthor.clear();
    filteredGenerator.clear();
    filteredDescription.clear();

    // Define new filter values
    foreach (QVariant filter, newFiltersList)
    {
        QString actionType = filter.toMap().value("action").toString();
        int filterType = filter.toMap().value("type").toInt();
        QString filterText = filter.toMap().value("text").toString();

        QStringList pair;
        pair << filterText << actionType;

        switch (filterType)
        {
        case 0: // post content
            filteredContent.append(pair);
            break;

        case 1: // author
            filteredAuthor.append(pair);
            break;

        case 2: // application (generator)
            filteredGenerator.append(pair);
            break;

        case 3: // activity description
            filteredDescription.append(pair);
            break;

        default:
            break;
        }
    }

    qDebug() << "FilterChecker() filters updated, with" << newFiltersList.length() << "filters";
}



/*
 * Return NoFiltering, FilterOut or Highlight
 *
 */
int FilterChecker::validateActivity(ASActivity *activity)
{
    int filtered = NoFiltering;  // Innocent until proven guilty!


    QString postTitle = activity->object()->getTitle();
    QString postContents = activity->object()->getContent();
    foreach (QStringList contents, filteredContent)
    {
        if (postContents.contains(contents.first(), Qt::CaseInsensitive)
         || postTitle.contains(contents.first(), Qt::CaseInsensitive))
        {
            qDebug() << "Filtering item because of Post Content:" << contents.first();
            filtered = contents.last().toInt();
        }
    }

    QString activityAuthorId = activity->author()->getId();
    QString objectAuthorId = activity->object()->author()->getId();
    foreach (QStringList authorId, filteredAuthor)
    {
        if (activityAuthorId.contains(authorId.first(), Qt::CaseInsensitive)
         || objectAuthorId.contains(authorId.first(), Qt::CaseInsensitive))
        {
            qDebug() << "Filtering item because of Author ID:" << authorId.first();
            filtered = authorId.last().toInt();
        }
    }


    QString activityGenerator = activity->getGenerator();
    foreach (QStringList generator, filteredGenerator)
    {
        if (activityGenerator.contains(generator.first(), Qt::CaseInsensitive))
        {
            qDebug() << "Filtering item because of Application (generator):" << generator.first();
            filtered = generator.last().toInt();
        }
    }

    // Remove links from activity description, so it's easier to write filtering rules
    QString activityDescription = MiscHelpers::htmlWithoutLinks(activity->getContent());
    foreach (QStringList description, filteredDescription)
    {
        if (activityDescription.contains(description.first(), Qt::CaseInsensitive))
        {
            qDebug() << "Filtering item because of Activity Description:" << description.first();
            filtered = description.last().toInt();
        }
    }


    return filtered;
}
