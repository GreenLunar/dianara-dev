/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "pumpcontroller.h"

PumpController::PumpController(QObject *parent) :  QObject(parent)
{
    this->userAgentString = "Dianara/1.3.4-dev";

    this->serverScheme = "https://"; // Default, unless --nohttps is used

    this->postsPerPageMain = 20;
    this->postsPerPageOther = 10;

    this->proxyUsesAuth = false;

    this->ignoreSslErrors = false;
    this->ignoreSslInImages = false;

    this->silentFollows = false;
    this->silentListsHandling = false;
    this->silentLikes = false;

    qDebug() << "PumpController: about to initialize QOAuth.\n"
             << "** If you built Dianara with Qt 5, you'll probably get a crash!";
    qoauth = new QOAuth::Interface(this);
    qoauth->setRequestTimeout(60000); // 1 minute timeout

    QSettings settings;
    this->clientID = settings.value("clientID").toString();
    this->clientSecret = settings.value("clientSecret").toString();
    qoauth->setConsumerKey(clientID.toLocal8Bit());
    qoauth->setConsumerSecret(clientSecret.toLocal8Bit());


    this->isApplicationAuthorized = settings.value("isApplicationAuthorized",
                                                   false).toBool();

    if (isApplicationAuthorized)
    {
        qDebug() << "Dianara is already authorized for user ID:"
                 << settings.value("userID").toString();

        this->token = settings.value("token").toString().toLocal8Bit();
        this->tokenSecret = settings.value("tokenSecret").toString().toLocal8Bit();

        qDebug() << "Using token" << token;
        qDebug() << "And token secret"
                 << tokenSecret.left(5) + "********** (hidden)";
    }



    connect(&nam, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(requestFinished(QNetworkReply*)));

    // Handle SSL errors; ignore for images if option is set,
    // or for everything if --ignoresslerrors is used
    connect(&nam, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)),
            this, SLOT(sslErrorsHandler(QNetworkReply*,QList<QSslError>)));


    this->userFollowingCount = 0;
    this->userFollowersCount = 0;

    this->initialDataStep = 0;
    this->initialDataAttempts = 0;


    initialDataTimer = new QTimer(this);
    initialDataTimer->setSingleShot(false); // Triggered constantly until stopped
    connect(initialDataTimer, SIGNAL(timeout()),
            this, SLOT(getInitialData()));


    this->addressCheckTimer = new QTimer(this);
    addressCheckTimer->setSingleShot(true);
    connect(addressCheckTimer, SIGNAL(timeout()),
            this, SLOT(onValidationTimeout()));

    this->addressCheckTimedOut = false;


    qDebug() << "PumpController created";
}


PumpController::~PumpController()
{
    qDebug() << "PumpController destroyed";
}


void PumpController::setProxyConfig(QNetworkProxy::ProxyType proxyType,
                                    QString hostname, int port,
                                    bool useAuth,
                                    QString user, QString password)
{
    QNetworkProxy proxy;
    proxy.setType(proxyType);
    proxy.setHostName(hostname);
    proxy.setPort(port);
    if (useAuth)
    {
        proxy.setUser(user);
        proxy.setPassword(password);

        // If using auth, and proxy type is set
        if (proxyType != QNetworkProxy::NoProxy)
        {
            this->proxyUsesAuth = true;
        }
    }
    nam.setProxy(proxy);
    qoauth->networkAccessManager()->setProxy(proxy);

    qDebug() << "Proxy config applied:" << hostname << port << user;
}

bool PumpController::needsProxyPassword()
{
    if (this->proxyUsesAuth && this->nam.proxy().password().isEmpty())
    {
        return true;
    }

    return false;
}

void PumpController::setProxyPassword(QString password)
{
    QNetworkProxy proxy = this->nam.proxy();
    proxy.setPassword(password);
    this->nam.setProxy(proxy);
    this->qoauth->networkAccessManager()->setProxy(proxy);
}


void PumpController::updateApiUrls()
{
    this->apiBaseUrl = this->serverScheme + this->serverURL
                       + "/api/user/" + this->userName;
    qDebug() << "Base API URL is:" << this->apiBaseUrl;
    this->apiFeedUrl = this->apiBaseUrl + "/feed";
}



void PumpController::setPostsPerPageMain(int ppp)
{
    this->postsPerPageMain = ppp;
    qDebug() << "PumpController: setting postsPerPage (main) to"
             << this->postsPerPageMain;
}

void PumpController::setPostsPerPageOther(int ppp)
{
    this->postsPerPageOther = ppp;
    qDebug() << "PumpController: setting postsPerPage (other) to"
             << this->postsPerPageOther;
}



/*
 * Set new user ID (user@domain.tld) and clear OAuth-related tokens/secrets
 *
 *
 */
void PumpController::setNewUserId(QString userID)
{
    this->userId = userID;
    QStringList splittedUserID = this->userId.split("@");
    this->userName = splittedUserID.first(); // get username, before @
    this->serverURL = splittedUserID.last(); // get URL, after @
    qDebug() << "Server URL to connect:" << serverURL
             << "; username:" << userName;

    this->clientID.clear();
    this->clientSecret.clear();
    this->token.clear();
    this->tokenSecret.clear();

    this->postsEverSeen.clear();


    this->isApplicationAuthorized = false;
    emit this->authorizationStatusChanged(isApplicationAuthorized);
}




/*
 * Get "pumpserver.org" and "user" from "user@pumpserver.org",
 * set OAuth token from Account dialog
 *
 */
void PumpController::setUserCredentials(QString userID)
{
    this->initialDataTimer->stop(); // Just in case it was running before


    this->userId = userID;
    QStringList splittedUserID = this->userId.split("@");
    this->userName = splittedUserID.first();
    this->serverURL = splittedUserID.last();
    qDebug() << "New userID is:" << this->userId;

    this->updateApiUrls();


    emit this->authorizationStatusChanged(isApplicationAuthorized);

    this->haveProfile = false;
    this->haveFollowing = false;
    this->haveFollowers = false;
    this->havePersonLists = false;
    this->haveMainTL = false;
    this->haveDirectTL = false;
    this->haveActivityTL = false;
    this->haveFavoritesTL = false;
    this->haveMainMF = false;
    this->haveDirectMF = false;
    this->haveActivityMF = false;

    this->initialDataStep = 0;
    this->initialDataAttempts = 0;

    // This will call getUserProfile(), getContactList(), several getFeed()...
    if (isApplicationAuthorized)
    {
        this->initialDataTimer->start(2000);  // start 2 seconds after setting the ID
                                              // (mainly on program startup)
        emit logMessage(tr("Authorized to use account %1. Getting initial data.")
                        .arg(this->userId));
    }
    else
    {
        emit logMessage(tr("There is no authorized account."));
    }
}



QString PumpController::currentUserId()
{
    return this->userId;
}

QString PumpController::currentUsername()
{
    return this->userName;
}

QString PumpController::currentServerScheme()
{
    return this->serverScheme;
}

QString PumpController::currentServerUrl()
{
    return this->serverURL;
}

QString PumpController::currentFollowersUrl()
{
    return this->userFollowersURL;
}

int PumpController::currentFollowersCount()
{
    return this->userFollowersCount;
}

int PumpController::currentFollowingCount()
{
    return this->userFollowingCount;
}

bool PumpController::currentlyAuthorized()
{
    return this->isApplicationAuthorized;
}




/*
 * Get any user's profile (not only our own)
 *
 * GET https://pumpserver.example/api/user/username
 *
 */
void PumpController::getUserProfile(QString userID)
{
    QStringList splittedUserID = userID.split("@");

    QString url = this->serverScheme + splittedUserID.last()
                  + "/api/user/" + splittedUserID.first();

    QNetworkRequest userProfileRequest = this->prepareRequest(url, QOAuth::GET,
                                                              UserProfileRequest);
    nam.get(userProfileRequest);

    qDebug() << "Requested user profile:" << userProfileRequest.url().toString();
}



/*
 * Update user's profile
 *
 */
void PumpController::updateUserProfile(QString avatarUrl, QString fullName,
                                       QString hometown, QString bio)
{
    QString url = this->apiBaseUrl + "/profile";
    QNetworkRequest updateProfileRequest = this->prepareRequest(url, QOAuth::PUT,
                                                                UpdateProfileRequest);


    QVariantMap jsonVariantImage;
    jsonVariantImage.insert("url", avatarUrl);
    jsonVariantImage.insert("width",  90);   // FIXME: don't hardcode this
    jsonVariantImage.insert("height", 90);   // get values from actual pixmap

    QVariantMap jsonVariantLocation;
    jsonVariantLocation.insert("objectType", "place");
    jsonVariantLocation.insert("displayName", hometown);

    QVariantMap jsonVariant;
    jsonVariant.insert("objectType", "person");

    if (!avatarUrl.isEmpty()) // Only add image object if a new image was uploaded
    {
        jsonVariant.insert("image", jsonVariantImage);
    }
    jsonVariant.insert("displayName", fullName);
    jsonVariant.insert("location", jsonVariantLocation);
    jsonVariant.insert("summary", bio);


    QByteArray data = this->prepareJSON(jsonVariant);
    nam.put(updateProfileRequest, data);
    qDebug() << "Updating user profile" << fullName << hometown;
}


/*
 * Update user's e-mail, used for notifications and such.
 * This might change in the future.
 *
 */
void PumpController::updateUserEmail(QString newEmail, QString password)
{
    QNetworkRequest updateEmailRequest = this->prepareRequest(apiBaseUrl,
                                                              QOAuth::PUT,
                                                              UpdateEmailRequest);

    QVariantMap jsonEmailVariant;
    jsonEmailVariant.insert("email",    newEmail);

    // Changing the e-mail requires providing username (inmutable) and password
    jsonEmailVariant.insert("nickname", this->userName);
    jsonEmailVariant.insert("password", password);

    QByteArray data = this->prepareJSON(jsonEmailVariant);
    nam.put(updateEmailRequest, data);
}


/*
 * Add an avatar URL to the queue of pending
 *
 */
void PumpController::enqueueAvatarForDownload(QString url)
{
    if (QFile::exists(MiscHelpers::getCachedAvatarFilename(url))
     || pendingAvatarsList.contains(url))
    {
        qDebug() << "PumpController() Using cached avatar, or it is pending download...";
    }
    else
    {
        this->pendingAvatarsList.append(url);
        this->getAvatar(url);
        qDebug() << "PumpController() Avatar not cached, downloading" << url;
    }
}



/*
 * Add the URL of an image to the queue of pending-download
 *
 * connect() signal/slot if necessary to refresh when done
 *
 */
void PumpController::enqueueImageForDownload(QString url)
{
    if (QFile::exists(MiscHelpers::getCachedImageFilename(url))
     || pendingImagesList.contains(url))
    {
        qDebug() << "PumpController::enqueueImageForDownload(), "
                    "Using cached image, or requested image is pending download...";
    }
    else
    {
        this->pendingImagesList.append(url);
        this->getImage(url);

        qDebug() << "PumpController::enqueueImageForDownload(), "
                    "image not cached, downloading" << url;
    }
}




void PumpController::getAvatar(QString avatarUrl)
{
    if (avatarUrl.isEmpty())
    {
        return;
    }

    qDebug() << "Getting avatar" << avatarUrl;

    QNetworkRequest avatarRequest(QUrl((const QString)avatarUrl));
    avatarRequest.setRawHeader("User-Agent", userAgentString);
    avatarRequest.setAttribute(QNetworkRequest::User,
                               QVariant(AvatarRequest));

    nam.get(avatarRequest);
}



void PumpController::getImage(QString imageUrl)
{
    if (imageUrl.isEmpty())
    {
        return;
    }

    QNetworkRequest imageRequest = this->prepareRequest(imageUrl,
                                                        QOAuth::GET,
                                                        ImageRequest);
    nam.get(imageRequest);
    qDebug() << "getImage() imageRequest sent";
}


QNetworkReply *PumpController::getMedia(QString mediaUrl)
{
    QNetworkRequest mediaRequest = this->prepareRequest(mediaUrl,
                                                        QOAuth::GET,
                                                        MediaRequest);

    qDebug() << "getMedia() sending mediaRequest for:" << mediaUrl;
    return nam.get(mediaRequest);
}



void PumpController::notifyAvatarStored(QString avatarUrl, QString avatarFilename)
{
    pendingAvatarsList.removeAll(avatarUrl);

    emit avatarStored(avatarUrl, avatarFilename);
}

void PumpController::notifyImageStored(QString imageUrl)
{
    pendingImagesList.removeAll(imageUrl);

    emit imageStored(imageUrl);
}


void PumpController::notifyImageFailed(QString imageUrl)
{
    pendingImagesList.removeAll(imageUrl);

    emit imageFailed(imageUrl);
}




/*
 * GET https://pumpserver.example/api/user/username/following or /followers
 *
 */
void PumpController::getContactList(QString listType, int offset)
{
    qDebug() << "Getting contact list, type" << listType << "; offset:" << offset;

    QString url = this->apiBaseUrl + "/" + listType;

    QOAuth::ParamMap paramMap;
    paramMap.insert("count",  "200"); // 200 each time (max allowed by API)
    paramMap.insert("offset", QString("%1").arg(offset).toLocal8Bit());


    QNetworkRequest contactListRequest;
    if (listType == "following")
    {
        contactListRequest = this->prepareRequest(url, QOAuth::GET,
                                                  FollowingListRequest,
                                                  paramMap);
        if (offset == 0)
        {
            totalReceivedFollowing = 0;
            followingIdList.clear();
            this->showStatusMessageAndLogIt(tr("Getting list of 'Following'..."));
        }
    }
    else
    {
        contactListRequest = this->prepareRequest(url, QOAuth::GET,
                                                  FollowersListRequest,
                                                  paramMap);
        if (offset == 0)
        {
            totalReceivedFollowers = 0;
        }
        this->showStatusMessageAndLogIt(tr("Getting list of 'Followers'..."));
    }

    nam.get(contactListRequest);
}


void PumpController::getSiteUserList()
{
    QString url = this->serverScheme + this->serverURL + "/api/users";

    QOAuth::ParamMap paramMap;
    paramMap.insert("count", "50"); // 50 users instead of the server default

    QNetworkRequest siteUsersRequest = this->prepareRequest(url, QOAuth::GET,
                                                            SiteUserListRequest,
                                                            paramMap);

    emit currentJobChanged(tr("Getting site users for %1...",
                              "%1 is a server name")
                           .arg(this->serverURL));
    nam.get(siteUsersRequest);
}


/*
 * Check if a userID is in the "following" list
 *
 */
bool PumpController::userInFollowing(QString contactId)
{
    if (followingIdList.contains(contactId))
    {
        return true;
    }
    else
    {
        return false;
    }
}


void PumpController::updateInternalFollowingIdList(QStringList idList)
{
    this->followingIdList.append(idList);
}

void PumpController::removeFromInternalFollowingList(QString id)
{
    this->followingIdList.removeAll(id);
}



/*
 * GET https://pumpserver.example/api/user/username/lists/person
 *
 */
void PumpController::getListsList()
{
    qDebug() << "Getting list of lists";

    QString url = this->apiBaseUrl + "/lists/person";

    QOAuth::ParamMap paramMap;
    paramMap.insert("count",  "200"); // Get up to 200 lists

    QNetworkRequest listsListRequest = this->prepareRequest(url, QOAuth::GET,
                                                            ListsListRequest,
                                                            paramMap);

    emit currentJobChanged(tr("Getting list of person lists..."));

    nam.get(listsListRequest);
}


/*
 * Create a person list
 *
 */
void PumpController::createPersonList(QString name, QString description)
{
    qDebug() << "PumpController() creating person list:" << name;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       CreatePersonListRequest);

    QVariantList jsonVariantObjectTypes;
    jsonVariantObjectTypes << "person";

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "collection");
    jsonVariantObject.insert("objectTypes", jsonVariantObjectTypes);
    jsonVariantObject.insert("displayName", name);
    jsonVariantObject.insert("content", description);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "create");
    jsonVariant.insert("object", jsonVariantObject);


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;


    emit currentJobChanged(tr("Creating person list..."));
    nam.post(postRequest, data);
}


void PumpController::deletePersonList(QString id)
{
    qDebug() << "PumpController::deletePersonList() deleting list" << id;

    QNetworkRequest deleteRequest = this->prepareRequest(id, QOAuth::DELETE,
                                                         DeletePersonListRequest);

    emit currentJobChanged(tr("Deleting person list..."));
    nam.deleteResource(deleteRequest);
}


void PumpController::getPersonList(QString url)
{
    qDebug() << "Getting a person list:" << url;

    QOAuth::ParamMap paramMap;
    paramMap.insert("count",  "200"); // Get 200 members // FIXME: could be more

    QNetworkRequest personListRequest = this->prepareRequest(url, QOAuth::GET,
                                                             PersonListRequest,
                                                             paramMap);
    emit currentJobChanged(tr("Getting a person list..."));

    nam.get(personListRequest);
}



/*
 * Add a new member to a list
 *
 */
void PumpController::addPersonToList(QString listId, QString personId)
{
    qDebug() << "PumpController() adding person to list:"
             << personId << listId;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       AddMemberToListRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "person");
    jsonVariantObject.insert("id", personId);

    QVariantMap jsonVariantTarget;
    jsonVariantTarget.insert("objectType", "collection");
    jsonVariantTarget.insert("id", listId);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "add");
    jsonVariant.insert("object", jsonVariantObject);
    jsonVariant.insert("target", jsonVariantTarget);
    if (this->silentListsHandling) // In 'private' mode, address to the same as the object
    {
        QVariantList jsonAudienceTo;
        jsonAudienceTo.append(jsonVariantObject); // To: only the specific user
        jsonVariant.insert("to", jsonAudienceTo);
    }


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;


    emit currentJobChanged(tr("Adding person to list..."));
    nam.post(postRequest, data);
}



/*
 * Remove member from a list
 *
 */
void PumpController::removePersonFromList(QString listId, QString personId)
{
    qDebug() << "PumpController() removing person from list:" << personId << listId;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       RemoveMemberFromListRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "person");
    jsonVariantObject.insert("id", personId);

    QVariantMap jsonVariantTarget;
    jsonVariantTarget.insert("objectType", "collection");
    jsonVariantTarget.insert("id", listId);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "remove");
    jsonVariant.insert("object", jsonVariantObject);
    jsonVariant.insert("target", jsonVariantTarget);
    if (this->silentListsHandling) // 'private' mode
    {
        QVariantList jsonAudienceTo;
        jsonAudienceTo.append(jsonVariantObject);
        jsonVariant.insert("to", jsonAudienceTo);
    }


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;


    emit currentJobChanged(tr("Removing person from list..."));
    nam.post(postRequest, data);
}



/*
 * Create a group where users can join and post messages for the other
 * members, similar to the StatusNet groups.
 *
 */
void PumpController::createGroup(QString name,
                                 QString summary,
                                 QString description)
{
    qDebug() << "PumpController() creating group:" << name;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       CreateGroupRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "group");
    if (!name.isEmpty())
    {
        jsonVariantObject.insert("displayName", name);
    }
    jsonVariantObject.insert("summary", summary);
    jsonVariantObject.insert("content", description);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "create");
    jsonVariant.insert("object", jsonVariantObject);


    // Audience, To:Public // Groups created public ATM -- FIXME
    QVariantMap jsonVariantPublic;
    jsonVariantPublic.insert("objectType", "collection");
    jsonVariantPublic.insert("id", "http://activityschema.org/collection/public");

    QVariantList jsonVariantAudience;
    jsonVariantAudience.append(jsonVariantPublic);
    jsonVariant.insert("to", jsonVariantAudience); // To: Public


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;

    emit currentJobChanged(tr("Creating group..."));
    nam.post(postRequest, data);
}


/*
 * Join a group to be able to send messages to it.
 * Joining based on ID.
 *
 */
void PumpController::joinGroup(QString id)
{
    qDebug() << "PumpController() joining group:" << id;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       JoinGroupRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "group");
    jsonVariantObject.insert("id", id);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "join");
    jsonVariant.insert("object", jsonVariantObject);


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;

    emit currentJobChanged(tr("Joining group..."));
    nam.post(postRequest, data);
}


void PumpController::leaveGroup(QString id)
{
    qDebug() << "PumpController() leaving group:" << id;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       LeaveGroupRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "group");
    jsonVariantObject.insert("id", id);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "leave");
    jsonVariant.insert("object", jsonVariantObject);


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;

    emit currentJobChanged(tr("Leaving group..."));
    nam.post(postRequest, data);
}




/*
 * Get list of people who liked a specific post
 *
 */
void PumpController::getPostLikes(QString postLikesUrl)
{
    qDebug() << "Getting likes for post" << postLikesUrl;

    emit currentJobChanged(tr("Getting likes..."));

    QOAuth::ParamMap paramMap;
    paramMap.insert("count", "100"); // TMP, up to 100 likes

    QNetworkRequest likesRequest = this->prepareRequest(postLikesUrl,
                                                        QOAuth::GET,
                                                        PostLikesRequest,
                                                        paramMap);
    nam.get(likesRequest);
}



/*
 * Get comments for one specific post
 *
 * GET https://pumpserver.example/api/#objectType#/#id#/replies
 * or proxyed URL. URL is given by the post itself
 *
 */
void PumpController::getPostComments(QString postCommentsUrl)
{
    qDebug() << "Getting comments for post" << postCommentsUrl;

    if (!urlIsInOurHost(postCommentsUrl)) // Post in another server and no proxyURL
    {
        emit currentJobChanged(tr("The comments for this post cannot be loaded "
                                  "due to missing data on the server."));
        return;
    }

    emit currentJobChanged(tr("Getting comments..."));

    QOAuth::ParamMap paramMap;
    paramMap.insert("count", "200"); // TMP, up to 200 comments / FIXME?

    QNetworkRequest commentsRequest = this->prepareRequest(postCommentsUrl,
                                                           QOAuth::GET,
                                                           PostCommentsRequest,
                                                           paramMap);
    nam.get(commentsRequest);
}


/*
 * Get list of people who shared a specific post
 *
 */
void PumpController::getPostShares(QString postSharesUrl)
{
    // TODO
}




/*
 * Get a feed
 *
 * Major timelines: "Timeline", "Messages", "Activity" and "Favorites"
 *
 * GET https://pumpserver.example/api/username/inbox/major
 *
 * /inbox/direct/major = Direct timeline, posts with the user's address in
 *                       the "To:" field, that is, sent explicitly to the user
 *
 * /feed/major = Activity timeline, user's own posts
 *
 * /favorites = Favorites timeline, posts where user clicked "like"
 *
 * Minor feeds: "Meanwhile", "Mentions" and "Actions"
 *
 * GET https://pumpserver.example/api/username/inbox/minor
 *                                            /inbox/direct/minor
 *                                            /feed/minor
 *
 */
void PumpController::getFeed(PumpController::requestTypes feedType,
                             int itemCount, QString url, int feedOffset)
{
    // Name of the feed and API path
    QStringList feedNameAndPath = this->getFeedNameAndPath(feedType);
    qDebug() << "PumpController::getFeed() " << feedNameAndPath;

    emit currentJobChanged(tr("Getting '%1'...",
                              "%1 is the name of a feed")
                           .arg(feedNameAndPath.first()));

    QOAuth::ParamMap paramMap;
    paramMap.insert("count", QString("%1").arg(itemCount).toLocal8Bit());
    if (feedOffset != 0)
    {
        paramMap.insert("offset", QString("%1").arg(feedOffset).toLocal8Bit());
    }

    if (url.isEmpty())
    {
        url = this->apiBaseUrl + feedNameAndPath.last();
    }
    else
    {
        if (url.contains("?")) // Only if there are parameters in the URL
        {
            // FIXME: This should be made safer
            QStringList splitUrl = url.split("?");
            url = splitUrl.first();
            QStringList splitParams = splitUrl.last().split("=");

            paramMap.insert(splitParams.first().toUtf8(),   // 'before' or 'since'
                            splitParams.last().toUtf8());   // An activity ID
        }
    }

    QNetworkRequest feedRequest = this->prepareRequest(url, QOAuth::GET,
                                                       feedType,
                                                       paramMap);
/*
    qDebug() << "PumpController::getFeed() ****\nfeedRequest:\n";
    qDebug() << feedRequest.rawHeader("Authorization");
    qDebug() << "\n*\n\nURL: " << feedRequest.url().toString() << "\n\n*******";
*/
    nam.get(feedRequest);
}

QStringList PumpController::getFeedNameAndPath(int feedType)
{
    QStringList nameAndPath;

    switch (feedType)
    {
    case MainTimelineRequest:
        nameAndPath << tr("Timeline")  << "/inbox/major";
        break;
    case DirectTimelineRequest:
        nameAndPath << tr("Messages")  << "/inbox/direct/major";
        break;
    case ActivityTimelineRequest:
        nameAndPath << tr("Activity")  << "/feed/major";
        break;
    case FavoritesTimelineRequest:
        nameAndPath << tr("Favorites") << "/favorites";
        break;

    case MinorFeedMainRequest:
        nameAndPath << tr("Meanwhile") << "/inbox/minor";
        break;
    case MinorFeedDirectRequest:
        nameAndPath << tr("Mentions")  << "/inbox/direct/minor";
        break;
    case MinorFeedActivityRequest:
        nameAndPath << tr("Actions")   << "/feed/minor";
        break;

    case UserTimelineRequest:
        nameAndPath << tr("User timeline") << ""; //// FIXME?
        break;

    default:
        nameAndPath << "" << "";
        qDebug() << "PumpController::getFeedNameAndPath() wrong feed type!";
    }

    return nameAndPath;
}


QString PumpController::getFeedApiUrl(int feedType)
{
    QString fullFeedUrl = this->apiBaseUrl;
    fullFeedUrl.append(this->getFeedNameAndPath(feedType).last());

    return fullFeedUrl;
}



/*
 * Prepare a QNetworkRequest with OAuth header, content type and user agent.
 *
 */
QNetworkRequest PumpController::prepareRequest(QString url,
                                               QOAuth::HttpMethod method,
                                               requestTypes requestType,
                                               QOAuth::ParamMap paramMap,
                                               QString contentTypeString)
{
    QByteArray authorizationHeader = qoauth->createParametersString(url,
                                                                 method,
                                                                 this->token,
                                                                 this->tokenSecret,
                                                                 QOAuth::HMAC_SHA1,
                                                                 paramMap,
                                                                 QOAuth::ParseForHeaderArguments);
    //qDebug() << "QOAuth::error()" << qoauth->error() << " (200=OK)";

    QNetworkRequest request;

    // Don't append inline parameters if they're empty, that can mess up things
    if (!paramMap.isEmpty())
    {
        url.append(qoauth->inlineParameters(paramMap,
                                            QOAuth::ParseForInlineQuery));
    }

    request.setUrl(QUrl(url));

    // Only add Authorization header if we're requesting something in our server
    if (request.url().host() == this->serverURL)
    {
        // Un-percent-encode the URL; needed to avoid "invalid signature" error
        // when parameters have special chars like % or /
        url = QByteArray::fromPercentEncoding(url.toLocal8Bit());
        request.setUrl(QUrl(url));

        request.setRawHeader("Authorization", authorizationHeader);
    }
    request.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeString);
    request.setRawHeader("User-Agent", userAgentString);

    request.setAttribute(QNetworkRequest::User, QVariant(requestType));

    return request;
}


/*
 * Generate JSON plaintext from a VarianMap.
 * Uses QJSON when building with Qt 4.x, and the built in Qt 5 method
 * when building with Qt 5.x.
 *
 */
QByteArray PumpController::prepareJSON(QVariantMap jsonVariantMap)
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    // Qt 4 version, using QJSON
    QJson::Serializer serializer;
    // bool ok;
    // QByteArray data = serializer.serialize(jsonVariantMap, &ok);

    // Without using the bool, to make it work with QJSON 0.7.x
    return serializer.serialize(jsonVariantMap);
#else
    // Qt 5 version, with built-in methods
    QJsonDocument jsonDocument;
    jsonDocument = QJsonDocument::fromVariant(jsonVariantMap);
    return jsonDocument.toJson();
#endif
}


/*
 * Read JSON and generate a QVariantMap
 * Uses QJSON when building with Qt 4.x, and built-in Qt 5 methods otherwise
 *
 */
QVariantMap PumpController::parseJSON(QByteArray rawData, bool *parsedOk)
{
    QVariantMap jsonData;
    bool parsed;

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    // Qt 4 version, requires QJSON
    QJson::Parser jsonParser;
    jsonData = jsonParser.parse(rawData, &parsed).toMap();
#else
    // Qt 5 version, built-in methods
    QJsonDocument jsonDocument;
    jsonDocument = QJsonDocument::fromJson(rawData);
    jsonData = jsonDocument.toVariant().toMap();
    parsed = !jsonDocument.isNull(); // tmp? FIXME
#endif

    *parsedOk = parsed;

    return jsonData;
}


/*
 * Upload a file to the /uploads feed for the user
 *
 * Used to upload pictures, audio, video and misc files
 *
 */
QNetworkReply *PumpController::uploadFile(QString filename,
                                          QString contentType,
                                          requestTypes uploadType)
{
    qDebug() << "PumpController::uploadFile()" << filename << contentType;

    QString url = this->apiBaseUrl + "/uploads";
    QNetworkRequest postRequest = this->prepareRequest(url, QOAuth::POST,
                                                       uploadType,
                                                       QOAuth::ParamMap(),
                                                       contentType);

    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QByteArray data = file.readAll(); // FIXME: This can use a lot of RAM with big files
    file.close();

    this->showStatusMessageAndLogIt(tr("Uploading %1",
                                       "1=filename").arg(filename)
                                    + QString(" (%1, %2)")
                                      .arg(contentType)
                                      .arg(MiscHelpers::fileSizeString(filename)));

    return nam.post(postRequest, data);
}



QList<QVariantList> PumpController::processAudience(QMap<QString, QString> audienceMap)
{
    QVariantList jsonVariantTo;
    QVariantList jsonVariantCC;

    QVariantMap jsonVariantAudienceItem;
    while (!audienceMap.isEmpty())
    {
        jsonVariantAudienceItem.clear();

        ///////////////////////////////////////////////////////////////// To:
        if (audienceMap.keys().contains("to|collection"))
        {
            QString collectionId = audienceMap.take("to|collection");
            jsonVariantAudienceItem.insert("objectType", "collection");
            jsonVariantAudienceItem.insert("id", collectionId);

            jsonVariantTo.append(jsonVariantAudienceItem);
        }
        else if (audienceMap.keys().contains("to|person"))
        {
            QString personId = audienceMap.take("to|person");
            jsonVariantAudienceItem.insert("objectType", "person");
            jsonVariantAudienceItem.insert("id", personId);

            jsonVariantTo.append(jsonVariantAudienceItem);
        }
        else if(audienceMap.keys().contains("to|group"))
        {
            QString groupId = audienceMap.take("to|group");
            jsonVariantAudienceItem.insert("objectType", "group");
            jsonVariantAudienceItem.insert("id", groupId);

            jsonVariantTo.append(jsonVariantAudienceItem);
        }
        ///////////////////////////////////////////////////////////////// Cc:
        else if (audienceMap.keys().contains("cc|collection"))
        {
            QString collectionId = audienceMap.take("cc|collection");
            jsonVariantAudienceItem.insert("objectType", "collection");
            jsonVariantAudienceItem.insert("id", collectionId);

            jsonVariantCC.append(jsonVariantAudienceItem);
        }
        else if (audienceMap.keys().contains("cc|person"))
        {
            QString personId = audienceMap.take("cc|person");
            jsonVariantAudienceItem.insert("objectType", "person");
            jsonVariantAudienceItem.insert("id", personId);

            jsonVariantCC.append(jsonVariantAudienceItem);
        }
        else if(audienceMap.keys().contains("cc|group"))
        {
            QString groupId = audienceMap.take("cc|group");
            jsonVariantAudienceItem.insert("objectType", "group");
            jsonVariantAudienceItem.insert("id", groupId);

            jsonVariantTo.append(jsonVariantAudienceItem);
        }
    }


    QList<QVariantList> jsonVariantToAndCCList;
    jsonVariantToAndCCList.append(jsonVariantTo);
    jsonVariantToAndCCList.append(jsonVariantCC);


    return jsonVariantToAndCCList;
}



bool PumpController::urlIsInOurHost(QString url)
{
    return (QUrl(url).host() == this->serverURL);
}


void PumpController::addCommentUrlToSeenList(QString id, QString url)
{
    if (urlIsInOurHost(url))
    {
        this->postsEverSeen.insert(id, url);
    }
}


QString PumpController::commentsUrlForPost(QString id)
{
    return this->postsEverSeen.value(id).toString();
}




void PumpController::showTransientMessage(QString message)
{
    emit transientStatusBarMessage(message);
}



void PumpController::showStatusMessageAndLogIt(QString message, QString url)
{
    emit currentJobChanged(message);
    emit logMessage(message, url);
}



/*
 * Show a message in the status bar with a snippet from a post or comment,
 * or its title, and add it to the log.
 *
 */
void PumpController::showObjectSnippetAndLogIt(QString message,
                                               QVariantMap jsonMap,
                                               QString messageWhenTitled)
{
    QString title = jsonMap.value("object").toMap()
                           .value("displayName").toString().trimmed();

    QString snippet;
    if (title.isEmpty())
    {
        QString content = jsonMap.value("object").toMap()
                                 .value("content").toString();
        snippet = MiscHelpers::htmlToPlainText(content, 40); // Limit to 40 chars
    }
    else
    {
        if (!messageWhenTitled.isEmpty())
        {
            message = messageWhenTitled;
        }

        snippet = title;
    }

    emit currentJobChanged(message.arg("\"" + snippet + "\""));
    emit logMessage(message.arg("<b>&quot;" + snippet + "&quot;</b>"));
}


void PumpController::setIgnoreSslErrors(bool state)
{
    this->ignoreSslErrors = state;
    this->qoauth->setIgnoreSslErrors(state);
}


void PumpController::setIgnoreSslInImages(bool state)
{
    this->ignoreSslInImages = state;
}

void PumpController::setNoHttpsMode()
{
    this->serverScheme = "http://"; // Instead of the default https://
    this->updateApiUrls(); // Update API base URL with this scheme
}



void PumpController::setSilentFollows(bool state)
{
    this->silentFollows = state;
}

void PumpController::setSilentLists(bool state)
{
    this->silentListsHandling = state;
}

void PumpController::setSilentLikes(bool state)
{
    this->silentLikes = state;
}



void PumpController::updatePostsEverSeen(QVariantMap postMap)
{
    this->postsEverSeen = postMap;
}


QVariantMap PumpController::getPostsEverSeen()
{
    return this->postsEverSeen;
}



/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
/*********************************** SLOTS *********************************/
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/




void PumpController::requestFinished(QNetworkReply *reply)
{
    int httpCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    int requestType = reply->request().attribute(QNetworkRequest::User).toInt();
    bool finished = reply->isFinished();
    QString replyUrl = reply->url().toString();
    QString replyHost = reply->url().host();
    QString replyErrorString = reply->errorString();
    QString replyServerVersion = reply->rawHeader("Server");
    QByteArray replyFirstLineRaw = reply->readLine();
    QByteArray replyData = replyFirstLineRaw + reply->readAll();
    QString replyFirstLine = QString(replyFirstLineRaw);
    QString prettyLogMessage;

    qDebug() << "Request finished. HTTP code:" << httpCode;
    qDebug() << "Size:" << replyData.length() << "bytes; URL:" << replyUrl;
    qDebug() << "isFinished()?" << finished << "; Request type:" << requestType;

    // We got all necessary data, clean up
    reply->deleteLater();


    // Special control after sending a post or a comment
    if (httpCode != 200) // if not OK
    {
        if (replyData.startsWith("{")) // Looks like JSON containing an error message
        {
            qDebug() << "Parsing JSON error message";
            bool jsonErrorParsed = false;
            QVariantMap jsonError = this->parseJSON(replyData, &jsonErrorParsed);

            if (jsonErrorParsed)
            {
                // Since it's a JSON-encoded error message, use it instead of
                // the first data line; These can have Unicode symbols
                QString jsonErrorString = jsonError.value("error").toString()
                                                                  .trimmed();
                if (!jsonErrorString.isEmpty())
                {
                    replyFirstLine = jsonErrorString;
                }
            }
        }


        if (requestType == PublishPostRequest)
        {
            emit postPublishingFailed();
        }
        else if (requestType == UpdatePostRequest)
        {
            emit postPublishingFailed();  // kinda TMP
        }
        else if (requestType == CommentPostRequest)
        {
            emit commentPostingFailed();
        }
        else if (requestType == UpdateCommentRequest)
        {
            emit commentPostingFailed(); // also, kinda TMP
        }
        else if (requestType == UploadMediaForPostRequest)
        {
            emit postPublishingFailed(); // FIXME?
        }
        else if (requestType == MediaRequest)
        {
            emit downloadFailed(replyUrl);
        }
        else if (requestType == ImageRequest)
        {
            this->notifyImageFailed(replyUrl);
        }
        else if (requestType == MainTimelineRequest
              || requestType == DirectTimelineRequest
              || requestType == ActivityTimelineRequest
              || requestType == FavoritesTimelineRequest)
        {
            emit currentJobChanged(tr("Error loading timeline!")); // FIXME: gets overwritten immediately
            emit timelineFailed(requestType);
        }
        else if (requestType == UserTimelineRequest)  // FIXME, merge with previous?
        {
            emit userTimelineFailed();
        }
        else if (requestType == MinorFeedMainRequest
              || requestType == MinorFeedDirectRequest
              || requestType == MinorFeedActivityRequest)
        {
            emit currentJobChanged(tr("Error loading minor feed!")); // FIXME: gets overwritten immediately
            emit minorFeedFailed(requestType);
        }
        else if (requestType == CheckContactRequest)
        {
            this->addressCheckTimer->stop(); // Timeout checking is irrelevant at this point

            qDebug() << "Contact ID is not valid, or server is down:" << replyUrl;
            this->showStatusMessageAndLogIt(tr("Unable to verify the address!")
                                            + " ("
                                            + this->addressPendingToFollow
                                            + ")");

            emit contactVerified(this->addressPendingToFollow,
                                 httpCode,
                                 this->addressCheckTimedOut,
                                 replyServerVersion);

            this->addressPendingToFollow.clear();

            return; // Avoid showing ugly 404 error
        }
    }


    const QString httpErrorString = tr("HTTP error",
                                       "For the following HTTP error codes"
                                       "you can check "
                                       "http://en.wikipedia.org/wiki/List_of_HTTP_status_codes "
                                       "in your language") + ": ";
    QString errorTypeString;

    switch (httpCode)
    {
    //////////////////////////////////////////////// First, handle error codes
    case 504:
        errorTypeString = httpErrorString
                        + tr("Gateway Timeout", "HTTP 504 error string")
                        + " (504)";
        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);
        qDebug() << "HTTP 504: Gateway Timeout.";
        qDebug() << "Data:  " << replyData;
        return;

    case 503:
        errorTypeString = httpErrorString
                        + tr("Service Unavailable", "HTTP 503 error string")
                        +  " (503) - " + replyFirstLine;

        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);

        // if not just an image, it's important, so popup notification too
        if (requestType != ImageRequest && requestType != AvatarRequest)
        {
            emit showErrorNotification(errorTypeString + "\n"
                                       + replyUrl);
        }

        qDebug() << "HTTP 503: Service Unavailable.";
        qDebug() << "Data:  " << replyData;
        return;

    case 501:
        errorTypeString = httpErrorString
                        + tr("Not Implemented", "HTTP 501 error string")
                        + " (501) - " + replyFirstLine;
        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);
        qDebug() << "HTTP 501: Not Implemented.";
        qDebug() << "Data:  " << replyFirstLineRaw;
        return;

    case 500:
        errorTypeString = httpErrorString
                        + tr("Internal Server Error", "HTTP 500 error string")
                        + " (500) - " + replyFirstLine;

        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);

        // if not just an image, it's important, so popup notification too
        if (requestType != ImageRequest && requestType != AvatarRequest)
        {
            emit showErrorNotification(errorTypeString + "\n"
                                       + replyUrl);
        }

        qDebug() << "HTTP 500: Internal Server Error.";
        qDebug() << "Data:  " << replyData;
        return;


    case 410:
        errorTypeString = httpErrorString
                        + tr("Gone", "HTTP 410 error string")
                        + " (410) " + replyFirstLine;
        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);
        qDebug() << "HTTP 410: Gone.";
        qDebug() << "Data:  " << replyFirstLineRaw;
        return;

    case 404:
        errorTypeString = httpErrorString
                        + tr("Not Found", "HTTP 404 error string")
                        + " (404)";
        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);
        qDebug() << "HTTP 404: Not Found.";
        qDebug() << "Data:  " << replyFirstLineRaw;
        return;

    case 403:
        errorTypeString = httpErrorString
                        + tr("Forbidden", "HTTP 403 error string")
                        + " (403) - " + replyFirstLine;
        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);
        qDebug() << "HTTP 403: Forbidden.";
        qDebug() << "Data:  " << replyFirstLineRaw;
        return;

    case 401:
        errorTypeString = httpErrorString
                        + tr("Unauthorized", "HTTP 401 error string")
                        + " (401) - " + replyFirstLine;
        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);
        qDebug() << "HTTP 401: Unauthorized.";
        qDebug() << "Data:  " << replyFirstLineRaw;
        return;


    case 400:
        errorTypeString = httpErrorString
                        + tr("Bad Request", "HTTP 400 error string")
                        + " (400) - " + replyFirstLine;

        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);

        // if not just an image, it's important, so popup notification too
        if (requestType != ImageRequest && requestType != AvatarRequest)
        {
            emit showErrorNotification(errorTypeString + "\n"
                                       + replyUrl);
        }

        qDebug() << "HTTP 400: Bad Request.";
        qDebug() << "Data:  " << replyData;
        return;

    case 302:
        errorTypeString = httpErrorString
                        + tr("Moved Temporarily", "HTTP 302 error string")
                        + " (302)";
        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);
        qDebug() << "HTTP 302: Moved Temporarily.";
        qDebug() << "Data:  " << replyFirstLineRaw;
        return;

    case 301:
        errorTypeString = httpErrorString
                        + tr("Moved Permanently", "HTTP 301 error string")
                        + " (301)";
        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);
        qDebug() << "HTTP 301: Moved Permanently.";
        qDebug() << "Data:  " << replyFirstLineRaw;
        return;

    case 0: // Other kinds of network errors
        errorTypeString = tr("Error connecting to %1").arg(replyHost);
        emit currentJobChanged(errorTypeString + ": " + replyErrorString);
        emit logMessage(errorTypeString + ": " + replyErrorString, replyUrl);

        qDebug() << "Error connecting to" << replyHost << ": "
                 << replyErrorString;
        return;

    // Other HTTP codes
    default:
        errorTypeString = tr("Unhandled HTTP error code %1").arg(httpCode);
        emit currentJobChanged(errorTypeString + ": " + replyUrl);
        emit logMessage(errorTypeString, replyUrl);
        qDebug() << "Unhandled HTTP error " << httpCode;
        qDebug() << "Data:  " << replyData;
        return;


    //////////////////////////////////////// The good one!
    case 200:
        qDebug() << "HTTP 200: OK!";
    }


    // At this point, httpCode should be 200 = OK



    // Stuff for JSON parsing
    bool jsonParsedOK = false;
    QVariantMap jsonData;
    QVariantList jsonDataList;


    // Unless it was an AvatarRequest, ImageRequest or MediaRequest,
    // it should be JSON, so parse it
    if (requestType != AvatarRequest
     && requestType != ImageRequest
     && requestType != MediaRequest)
    {
        jsonData = this->parseJSON(replyData, &jsonParsedOK);

        qDebug() << "JSON data size (items):" << jsonData.size();
        qDebug() << "Keys:" << jsonData.keys();
    }


    //////////////////////////////////////////////////////////////////

    switch (requestType)
    {

    case ClientRegistrationRequest:
        qDebug() << "Client Registration was requested";

        qDebug() << "Raw JSON:" << jsonData;

        if (jsonParsedOK && jsonData.size() > 0)
        {
            this->clientID = jsonData["client_id"].toString();
            this->clientSecret = jsonData["client_secret"].toString();

            // FIXME: error control, etc.
            // check if jsonData.keys().contains("client_id") !!

            QSettings settings;
            settings.setValue("clientID",     this->clientID);
            settings.setValue("clientSecret", this->clientSecret);
            settings.sync();

            this->getToken();
        }

        break;



    case UserProfileRequest:
        qDebug() << "A user profile was requested";

        if (jsonParsedOK && jsonData.size() > 0)
        {
            QVariantMap profileMap = jsonData["profile"].toMap();
            if (profileMap.value("id").toString() == "acct:" + this->userId)
            {
                qDebug() << "Received OWN profile; keys:" << profileMap.keys();
                qDebug() << "Links from profile:" << profileMap.value("links").toMap().keys();
                qDebug() << "Lists from profile:" << profileMap.value("lists").toMap()
                                                               .value("totalItems").toInt();
                qDebug() << "Lists URL:" << profileMap.value("lists").toMap()
                                                      .value("url").toString();
                this->haveProfile = true;

                QString profImageUrl = profileMap.value("image").toMap()
                                                 .value("url").toString();
                QString profDisplayName = profileMap.value("displayName").toString();
                QString profLocation = profileMap.value("location").toMap()
                                                 .value("displayName").toString();
                QString profSummary = profileMap.value("summary").toString();

                QString userEmail = jsonData["email"].toString();
                qDebug() << "E-mail configured for the account:" << userEmail;

                emit profileReceived(profImageUrl, profDisplayName,
                                     profLocation, profSummary,
                                     userEmail);

                // Store also the user's followers URL, for posting to Followers
                this->userFollowersURL = profileMap.value("followers").toMap()
                                                   .value("url").toString();

                this->userFollowersCount = profileMap.value("followers").toMap()
                                                     .value("totalItems").toInt();
                this->userFollowingCount = profileMap.value("following").toMap()
                                                     .value("totalItems").toInt();

                qDebug() << "Followers count:" << userFollowersCount;
                qDebug() << "Following count:" << userFollowingCount;

                this->showStatusMessageAndLogIt(tr("Profile received.")
                                                + " "
                                                + tr("Followers")
                                                + QString(": %1;")
                                                  .arg(QLocale::system()
                                                       .toString(userFollowersCount))
                                                + " "
                                                + tr("Following")
                                                + QString(": %1")
                                                  .arg(QLocale::system()
                                                       .toString(userFollowingCount)));
            }
            else
            {
                qDebug() << "Expected profile information; got:";
                qDebug() << replyData;
            }
        }

        break;


    case UpdateProfileRequest:
        this->showStatusMessageAndLogIt(tr("Profile updated."));
        this->getUserProfile(this->userId);

        emit userDidSomething();

        break;


    case UpdateEmailRequest:
        this->showStatusMessageAndLogIt(tr("E-mail updated: %1")
                                        .arg(jsonData.value("email")
                                                     .toString()));

        this->getUserProfile(this->userId); // TMP, FIXME...

        break;



    ////////////////////////////////////
    case PublishPostRequest:
        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";

            QString objectId = jsonData.value("object").toMap()
                                       .value("id").toString();
            qDebug() << "Post ID:" << objectId;

            QString objectType = jsonData.value("object").toMap()
                                         .value("objectType").toString();
            if (objectType == "image"
             || objectType == "audio"
             || objectType == "video"
             || objectType == "file")
            {
                QString translatedObjectType = ASObject::getTranslatedType(objectType);
                this->showStatusMessageAndLogIt(tr("%1 published successfully. "
                                                   "Updating post content...",
                                                   "%1 is the type of object: "
                                                   "note, image...")
                                                .arg(translatedObjectType));

                // Update the object with title and description
                // (workaround for the non-title-non-description issue)
                this->updatePost(objectId,
                                 objectType,
                                 this->currentPostDescription,
                                 this->currentPostTitle);
            }
            else
            {
                // Not a a media post, notify "posted OK"
                this->showObjectSnippetAndLogIt(tr("Untitled post %1 "
                                                   "published successfully.",
                                                   "%1 is a piece of the post"),
                                                jsonData,
                                                tr("Post %1 published successfully.",
                                                   "%1 is the title of the post"));
                emit postPublished();
                qDebug() << "Non-media post published correctly";
            }
        }
        else
        {
            qDebug() << "Error parsing received JSON data!";
            qDebug() << "Raw data:" << replyData; // JSON directly
        }

        break;



    case PublishAvatarRequest:
        this->showStatusMessageAndLogIt(tr("Avatar published successfully."));
        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";

            QString imageUrl = jsonData.value("object").toMap().value("image")
                                                       .toMap().value("url").toString();
            qDebug() << "Avatar post ID:" << imageUrl;
            // FIXME: get "pump_io: fullImage: url" too

            if (jsonData["object"].toMap().value("objectType").toString() == "image")
            {
                emit avatarUploaded(imageUrl);
            }
            else
            {
                qDebug() << "Avatar uploaded, but type is not IMAGE!";
            }
        }
        else
        {
            qDebug() << "Error parsing received JSON data!";
            qDebug() << "Raw data:" << replyData; // JSON directly
        }

        break;


    ////////////////////////////////////
    case UpdatePostRequest:
        if (jsonParsedOK && jsonData.size() > 0)
        {
            this->showObjectSnippetAndLogIt(tr("Untitled post %1 "
                                               "updated successfully.",
                                               "%1 is a piece of the post"),
                                            jsonData,
                                            tr("Post %1 updated successfully.",
                                               "%1 is the title of the post"));
        }

        emit postPublished();
        break;

    case UpdateCommentRequest:
        if (jsonParsedOK && jsonData.size() > 0)
        {
            this->showObjectSnippetAndLogIt(tr("Comment %1 updated successfully.",
                                               "%1 is a piece of the comment"),
                                            jsonData);
        }

        emit commentPosted();
        emit userDidSomething();
        break;




    ///////////////////////////////////// If liking a post was requested
    case LikePostRequest:
        this->showStatusMessageAndLogIt(tr("Message liked or unliked "
                                           "successfully."));
        emit likeSet();
        emit userDidSomething();
        break;


    ///////////////////////////////////// If the likes for a post were requested
    case PostLikesRequest:
        qDebug() << "Likes for a post were requested" << replyUrl;

        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";
            emit currentJobChanged(tr("Likes received."));

            jsonDataList = jsonData["items"].toList();
            qDebug() << "Number of items in comments list:" << jsonDataList.size();

            emit likesReceived(jsonDataList, replyUrl);
        }
        else
        {
            qDebug() << "Error parsing received comment JSON data!";
            qDebug() << "Raw data:" << replyData;
        }
        break;



    ///////////////////////////////////// If commenting on a post was requested
    case CommentPostRequest:
        if (jsonParsedOK && jsonData.size() > 0)
        {
            this->showObjectSnippetAndLogIt(tr("Comment %1 posted successfully.",
                                               "%1 is a piece of the comment"),
                                            jsonData);
        }

        emit commentPosted(); // This will be caught by Commenter()
        emit userDidSomething();
        break;



    ///////////////////////////////////// If the comments for a post were requested
    case PostCommentsRequest:
        qDebug() << "Comments for a post were requested" << replyUrl;

        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";
            jsonDataList = jsonData["items"].toList();

            int commentCount = jsonDataList.size();
            qDebug() << "Number of items in comments list:" << commentCount;

            if (commentCount == 1)
            {
                emit currentJobChanged(tr("1 comment received."));
            }
            else
            {
                emit currentJobChanged(tr("%1 comments received.")
                                       .arg(commentCount));
            }

            emit commentsReceived(jsonDataList, replyUrl);
        }
        else
        {
            qDebug() << "Error parsing received comment JSON data!";
            qDebug() << "Raw data:" << replyData;
        }
        break;



    case SharePostRequest:
        qDebug() << "Post shared OK";

        if (jsonParsedOK && jsonData.size() > 0)
        {
            // FIXME: this should be done using ASActivity+ASObject
            QVariantMap authorMap = jsonData.value("object").toMap().value("author").toMap();
            QString authorName = authorMap.value("displayName").toString();
            if (authorName.isEmpty())
            {
                authorName = ASPerson::cleanupId(authorMap.value("id").toString());
            }
            this->showStatusMessageAndLogIt(tr("Post by %1 shared successfully.",
                                               "1=author of the post we are sharing")
                                            .arg(authorName));

            emit userDidSomething();
        }

        break;

    case PostSharesRequest:
        // TODO
        break;



    /////////////////////////////////////////////// If a feed was requested
    case MainTimelineRequest:
        // just jump to the next
    case DirectTimelineRequest:
        // just jump to next
    case ActivityTimelineRequest:
        // just... yeah, jump
    case FavoritesTimelineRequest:
        // and jump!
    case MinorFeedMainRequest:
        // jump!
    case MinorFeedDirectRequest:
        // jump, jump, jump!!
    case MinorFeedActivityRequest:
        // everybody jump!
    case UserTimelineRequest:
        prettyLogMessage = tr("Received '%1'.",
                              "%1 is the name of a feed")
                           .arg(this->getFeedNameAndPath(requestType).first());

        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";

            jsonDataList = jsonData["items"].toList();
            qDebug() << "Number of items in this feed block:"
                     << jsonDataList.size();
            if (jsonDataList.size() > 0)
            {
                prettyLogMessage.append(" " + tr("Adding items..."));
            }

            // Emit this before the other signals
            emit currentJobChanged(prettyLogMessage);


            int totalItems = jsonData.value("totalItems").toInt();
            QString previousLink = jsonData.value("links").toMap()
                                           .value("prev").toMap()
                                           .value("href").toString();
            QString nextLink = jsonData.value("links").toMap()
                                       .value("next").toMap()
                                       .value("href").toString();

            if (requestType == MainTimelineRequest)
            {
                qDebug() << "It was the main timeline";
                this->haveMainTL = true;
                emit mainTimelineReceived(jsonDataList, previousLink,
                                          nextLink, totalItems);
            }
            else if (requestType == DirectTimelineRequest)
            {
                qDebug() << "It was the direct messages timeline";
                this->haveDirectTL = true;
                emit directTimelineReceived(jsonDataList, previousLink,
                                            nextLink, totalItems);
            }
            else if (requestType == ActivityTimelineRequest)
            {
                qDebug() << "It was the own activity timeline";
                this->haveActivityTL = true;
                emit activityTimelineReceived(jsonDataList, previousLink,
                                              nextLink, totalItems);
            }
            else if (requestType == FavoritesTimelineRequest)
            {
                qDebug() << "It was the favorites timeline";
                this->haveFavoritesTL = true;
                emit favoritesTimelineReceived(jsonDataList, previousLink,
                                               nextLink, totalItems);
            }
            else if (requestType == UserTimelineRequest)
            {
                QString timelineUrl = jsonData.value("url").toString();
                qDebug() << "It was a user timeline:" << timelineUrl;
                emit userTimelineReceived(jsonDataList, previousLink,
                                          nextLink, totalItems,
                                          timelineUrl);
            }
            else if (requestType == MinorFeedMainRequest)
            {
                qDebug() << "It was the Meanwhile feed";
                this->haveMainMF = true;
                emit minorFeedMainReceived(jsonDataList, previousLink,
                                           nextLink, totalItems);
            }
            else if (requestType == MinorFeedDirectRequest)
            {
                qDebug() << "It was the Mentions feed";
                this->haveDirectMF = true;
                emit minorFeedDirectReceived(jsonDataList, previousLink,
                                             nextLink, totalItems);
            }
            else if (requestType == MinorFeedActivityRequest)
            {
                qDebug() << "It was the Actions feed";
                this->haveActivityMF = true;
                emit minorFeedActivityReceived(jsonDataList, previousLink,
                                               nextLink, totalItems);
            }
        }
        else
        {
            qDebug() << "Error parsing received JSON data!";
            qDebug() << "Raw data:" << replyData; // JSON directly
        }
        break;



    case DeletePostRequest:
        this->showStatusMessageAndLogIt(tr("Message deleted successfully."));
        emit userDidSomething();

        break;


    case CheckContactRequest:
        this->addressCheckTimer->stop(); // No need to check timeout at this point

        qDebug() << "Contact ID seems OK:" << replyUrl;
        emit contactVerified(this->addressPendingToFollow, httpCode,
                             this->addressCheckTimedOut, replyServerVersion);

        this->addressPendingToFollow.clear();

        break;


    case FollowContactRequest:
        // Just jump to next
    case UnfollowContactRequest:
        if (jsonParsedOK && jsonData.size() > 0)
        {
            ASPerson *contact = new ASPerson(jsonData.value("object").toMap(),
                                             this);

            if (requestType == FollowContactRequest)
            {
                prettyLogMessage = tr("Following %1 (%2) successfully.",
                                      "%1 is a person's name, %2 is the ID")
                                   .arg(contact->getName())
                                   .arg(contact->getId());

                emit contactFollowed(contact);
                emit followingListChanged();
            }
            else
            {
                prettyLogMessage = tr("Stopped following %1 (%2) successfully.",
                                      "%1 is a person's name, %2 is the ID")
                                   .arg(contact->getName())
                                   .arg(contact->getId());

                emit contactUnfollowed(contact);
                emit followingListChanged();
            }

            this->showStatusMessageAndLogIt(prettyLogMessage);
            emit userDidSomething();
        }

        break;


    case FollowingListRequest:
        // just go to the next
    case FollowersListRequest:
        qDebug() << "A contact list was requested";

        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";

            QVariant contactsVariant = jsonData.value("items");
            int totalCollectionCount = jsonData.value("totalItems").toInt();
            if (contactsVariant.type() == QVariant::List)
            {
                qDebug() << "Parsed a List, listing contacts...";

                int receivedItemsCount = contactsVariant.toList().size();
                QString batchInfoString;

                if (requestType == FollowingListRequest)
                {
                    this->userFollowingCount = totalCollectionCount;
                    totalReceivedFollowing += receivedItemsCount;

                    emit contactListReceived("following",
                                             contactsVariant.toList(),
                                             this->totalReceivedFollowing);

                    batchInfoString = QString(" (%1/%2)")
                                      .arg(totalReceivedFollowing)
                                      .arg(userFollowingCount);

                    if (totalReceivedFollowing >= totalCollectionCount)
                    {
                        this->haveFollowing = true;
                        this->showStatusMessageAndLogIt(tr("List of 'following' "
                                                           "completely received.")
                                                        + batchInfoString);

                        emit followingListChanged();
                    }
                    else
                    {
                        emit currentJobChanged(tr("Partial list of 'following' "
                                                  "received.")
                                               + batchInfoString);
                        qDebug() << "Partial following received:"
                                 << batchInfoString;
                    }
                }
                else // == FollowersListRequest
                {
                    this->userFollowersCount = totalCollectionCount;
                    totalReceivedFollowers += receivedItemsCount;

                    emit contactListReceived("followers",
                                             contactsVariant.toList(),
                                             this->totalReceivedFollowers);

                    batchInfoString = QString(" (%1/%2)")
                                      .arg(totalReceivedFollowers)
                                      .arg(userFollowersCount);

                    if (totalReceivedFollowers >= totalCollectionCount)
                    {
                        this->haveFollowers = true;
                        this->showStatusMessageAndLogIt(tr("List of 'followers' "
                                                           "completely received.")
                                                        + batchInfoString);
                    }
                    else
                    {
                        emit currentJobChanged(tr("Partial list of 'followers' "
                                                  "received.")
                                               + batchInfoString);
                        qDebug() << "Partial following received:"
                                 << batchInfoString;
                    }
                }
            }
            else
            {
                qDebug() << "Expected a list of contacts, received something else:";
                qDebug() << jsonData;
            }
        }
        else
        {
            qDebug() << "Error parsing received JSON data!";
            qDebug() << "Raw data:" << replyData; // JSON directly
        }

        break;



    case ListsListRequest:
        qDebug() << "The list of person lists was requested";

        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";

            QVariant listsVariant = jsonData.value("items");
            if (listsVariant.type() == QVariant::List)
            {
                qDebug() << "Parsed a List, listing lists...";

                this->havePersonLists = true;
                emit currentJobChanged(tr("List of 'lists' received."));
                emit listsListReceived(listsVariant.toList());
            }
            else
            {
                qDebug() << "Expected a list of lists, received something else:";
                qDebug() << jsonData;
            }
        }
        else
        {
            qDebug() << "Error parsing received JSON data!";
            qDebug() << "Raw data:" << replyData; // JSON directly
        }

        break;



    case SiteUserListRequest:
        this->showStatusMessageAndLogIt(tr("List of %1 users received.",
                                           "%1 is a server name")
                                        .arg(this->serverURL));

        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";
            emit siteUserListReceived(jsonData.value("items").toList(),
                                      jsonData.value("totalItems").toInt());
            //qDebug() << "\n\n*** Site User List:\n\n" << replyData;
        }

        break;


    ///////////////////////////////////////////////////////////////////// Lists

    // Person list created OK
    case CreatePersonListRequest:
        this->showStatusMessageAndLogIt(tr("Person list '%1' created successfully.")
                                        .arg(jsonData.value("object").toMap()
                                                     .value("displayName").toString()));

        this->getListsList(); // And reload the person lists (FIXME!)
        emit userDidSomething();
        break;


    // Person list deleted OK
    case DeletePersonListRequest:
        this->showStatusMessageAndLogIt(tr("Person list deleted successfully."));

        this->getListsList(); // And reload the person lists (FIXME!)
        emit userDidSomething();
        break;


    case PersonListRequest:
        qDebug() << "A person list was requested";

        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";

            QVariant personListVariant = jsonData.value("items");
            if (personListVariant.type() == QVariant::List)
            {
                qDebug() << "Parsed a List, listing people in list..."
                         << jsonData.value("displayName").toString();

                emit currentJobChanged(tr("Person list received."));

                // Using list URL as ID, since the ID isn't provided here...
                emit personListReceived(personListVariant.toList(),
                                        jsonData.value("url").toString());
            }
            else
            {
                qDebug() << "Expected a list of people, received something else:";
                qDebug() << jsonData;
            }
        }
        else
        {
            qDebug() << "Error parsing received JSON data!";
            qDebug() << "Raw data:" << replyData; // JSON directly
        }

        break;


    // Person added to list OK
    case AddMemberToListRequest:
        if (jsonParsedOK && jsonData.size() > 0)
        {
            QString personId = ASPerson::cleanupId(jsonData.value("object").toMap()
                                                           .value("id").toString());
            QString personName = jsonData.value("object").toMap()
                                         .value("displayName").toString();
            QString personAvatar = jsonData.value("object").toMap()
                                           .value("image").toMap()
                                           .value("url").toString();

            emit personAddedToList(personId, personName, personAvatar);

            this->showStatusMessageAndLogIt(tr("%1 (%2) added to list successfully.",
                                               "1=contact name, 2=contact ID")
                                            .arg(personName)
                                            .arg(personId));
            emit userDidSomething();
        }
        break;


    // Person removed from list OK
    case RemoveMemberFromListRequest:
        if (jsonParsedOK && jsonData.size() > 0)
        {
            QString personId = ASPerson::cleanupId(jsonData.value("object").toMap()
                                                            .value("id").toString());
            QString personName = jsonData.value("object").toMap()
                                         .value("displayName").toString();

            emit personRemovedFromList(personId);

            this->showStatusMessageAndLogIt(tr("%1 (%2) removed from list successfully.",
                                               "1=contact name, 2=contact ID")
                                            .arg(personName)
                                            .arg(personId));
            emit userDidSomething();
        }
        break;


    //////////////////////////////////////////////////////////////////// Groups
    case CreateGroupRequest:
        this->showStatusMessageAndLogIt(tr("Group %1 created successfully.")
                                        .arg(jsonData.value("object").toMap()
                                                     .value("displayName").toString()));
        emit userDidSomething();

        qDebug() << "Group created; ID:" << jsonData.value("object").toMap()
                                                    .value("id").toString();
        break;


    case JoinGroupRequest:
        this->showStatusMessageAndLogIt(tr("Group %1 joined successfully.")
                                        .arg(jsonData.value("object").toMap()
                                                     .value("displayName").toString()));
        emit userDidSomething();

        qDebug() << "Group joined; ==========================";
        qDebug() << "Keys: " << jsonData.keys();
        qDebug() << jsonData.value("object").toString();

        break;


    case LeaveGroupRequest:
        this->showStatusMessageAndLogIt(tr("Left the %1 group successfully.")
                                        .arg(jsonData.value("object").toMap()
                                                     .value("displayName").toString()));
        emit userDidSomething();

        break;



    case AvatarRequest:
        qDebug() << "Received AVATAR data, from " << replyUrl;
        if (finished)
        {
            qDebug() << "Avatar received 100%";
            emit avatarPictureReceived(replyData, replyUrl);
        }
        else
        {
            qDebug() << "Avatar not complete yet";
        }
        break;


    case ImageRequest:
        qDebug() << "Received IMAGE data, from " << replyUrl;
        if (finished)
        {
            qDebug() << "Image received 100%";
            emit imageReceived(replyData, replyUrl);
        }
        else
        {
            qDebug() << "Image not complete yet";
        }
        break;


    case MediaRequest:
        qDebug() << "Received MEDIA data, from " << replyUrl;
        if (finished)
        {
            this->showStatusMessageAndLogIt(tr("File downloaded successfully."));

            emit downloadCompleted(replyUrl);

            qDebug() << "Media received 100%" << replyData.length() << "bytes";
        }
        else
        {
            qDebug() << "Media not complete yet";
        }
        break;


    //////////////////////////////////////// If uploading a file was requested
    case UploadAvatarRequest:
        // just jump to next
    case UploadMediaForPostRequest:
        // just jump
    case UploadFileRequest:
        qDebug() << "Uploading a file was requested";

        if (jsonParsedOK && jsonData.size() > 0)
        {
            qDebug() << "JSON parsed OK";

            QString uploadedFileId = jsonData["id"].toString();
            qDebug() << "Uploaded file ID:" << uploadedFileId;


            QString objectType = jsonData["objectType"].toString();
            if (objectType == "image"
             || objectType == "audio"
             || objectType == "video"
             || objectType == "file")
            {
                if (requestType == UploadMediaForPostRequest)
                {
                    prettyLogMessage = tr("File uploaded successfully. "
                                          "Posting message...");
                    emit currentJobChanged(prettyLogMessage);
                    emit logMessage(prettyLogMessage, replyUrl);

                    this->postMediaStepTwo(uploadedFileId);
                }
                else if (requestType == UploadAvatarRequest)
                {
                    this->showStatusMessageAndLogIt(tr("Avatar uploaded."));
                    this->postAvatarStepTwo(uploadedFileId);
                }
            }
        }
        else
        {
            qDebug() << "Error parsing received JSON data!";
            qDebug() << "Raw data:" << replyData; // JSON directly
        }

        break;


    }
    // end switch (requestType)

    qDebug() << "requestFinished() ended; " << replyUrl;
}



/*
 * Handle SSL errors
 *
 * Default is blocking connections with SSL problems, unless the request
 * if for an image on a remote host and the corresponding option was set
 * in the Post settings, or --ignoresslerrors was used
 *
 */
void PumpController::sslErrorsHandler(QNetworkReply *reply,
                                      QList<QSslError> errorList)
{
    qDebug() << "\n==== SSL errors!! ====";
    qDebug() << "At:" << reply->url().toString();
    qDebug() << "Error list:" << errorList << "\n\n";

    QString errorsString;
    foreach (QSslError sslError, errorList)
    {
        errorsString.append(sslError.errorString() + "; ");
    }
    errorsString.remove(-2, 2); // remove "; " at the end

    this->showStatusMessageAndLogIt(tr("SSL errors in connection to %1!")
                                    .arg(reply->url().host())
                                    + QString(" (%1)").arg(errorsString));

    bool allowLoadingImage = false;
    if (this->ignoreSslInImages)
    {
        int requestType = reply->request().attribute(QNetworkRequest::User).toInt();
        if (requestType == ImageRequest
         && reply->url().host() != this->serverURL)
        {
            allowLoadingImage = true;
            this->showStatusMessageAndLogIt(tr("Loading external image from "
                                               "%1 regardless of SSL errors, "
                                               "as configured...",
                                               "%1 is a hostname")
                                            .arg(reply->url().host()),
                                            reply->url().toString());
        }
    }

    // Ignore SSL errors and continue, if configured to do so
    if (this->ignoreSslErrors || allowLoadingImage)
    {
        qDebug() << "Ignoring these errors...";
        reply->ignoreSslErrors();
    }
}




void PumpController::getToken()
{
    // If we do not have client_id or client_secret, do dynamic client registration
    if (this->clientID.isEmpty() || this->clientSecret.isEmpty())
    {
        qDebug() << "PumpController::getToken()";
        qDebug() << "We do not have client_id/client_secret yet; "
                    "doing Dynamic Client Registration";

        this->showStatusMessageAndLogIt(tr("The application is not registered with "
                                           "your server yet. Registering..."));

        // POST to https://yourserver.example/api/client/register
        QNetworkRequest postRequest(QUrl(this->serverScheme + this->serverURL
                                         + "/api/client/register"));
        postRequest.setHeader(QNetworkRequest::ContentTypeHeader,
                              "application/json");
        postRequest.setRawHeader("User-Agent", userAgentString);
        postRequest.setAttribute(QNetworkRequest::User,
                                 QVariant(ClientRegistrationRequest));

        QByteArray data("{"
                        " \"type\": \"client_associate\",  "
                        " \"application_type\": \"native\", "
                        " \"application_name\": \"Dianara\", "
                        " \"logo_uri\": \"http://dianara.nongnu.org/dianara-logo.png\", "
                        " \"client_uri\": \"https://jancoding.wordpress.com/dianara\" "
                        "}");

        qDebug() << "About to POST:" << data;


        // upon receiving data (id+secret), will execute getToken() again
        nam.post(postRequest, data);
    }
    else
    {
        qDebug() << "Using saved client_id and client_secret:"
                 << this->clientID << this->clientSecret;

        // OAuth stuff.....
        // 1. obtaining an unauthorized Request Token from the Service Provider,
        // 2. asking the User to authorize the Request Token,
        // 3. exchanging the Request Token for the Access Token


        this->showStatusMessageAndLogIt(tr("Getting OAuth token..."));

        qDebug() << "Doing OAuth token stuff...";
        qDebug() << "NOTE: if you see a crash here, you need QCA and its "
                    "openSSL plugin:";
        qDebug() << ">>> qca2-plugin-openssl, libqca2-plugin-ossl, or similar";
        qDebug() << "If you compiled Dianara from source, check the INSTALL "
                    "file carefully";

        QStringList QCAsupportedFeatures = QCA::supportedFeatures();
        qDebug() << "QCA Supported Features:" << QCAsupportedFeatures;

        if (QCAsupportedFeatures.contains("hmac(sha1)"))
        {
            qDebug() << "HMAC-SHA1 support is OK";
        }
        else
        {
            qDebug() << "Warning, HMAC-SHA1 doesn't seem to be supported!";

            // Notify the user about missing plugin
            emit authorizationFailed(tr("OAuth support error"),
                                     tr("Your installation of QOAuth, a library "
                                        "used by Dianara, doesn't seem to have "
                                        "HMAC-SHA1 support.")
                                     + "\n"
                                     + tr("You probably need to install the OpenSSL "
                                          "plugin for QCA: %1, %2 or similar.")
                                       .arg("qca2-plugin-openssl")
                                       .arg("libqca2-plugin-ossl"));
        }

        qoauth->setConsumerKey(this->clientID.toLocal8Bit());
        qoauth->setConsumerSecret(this->clientSecret.toLocal8Bit());


        QString requestTokenUrl = this->serverScheme + this->serverURL
                                  + "/oauth/request_token";
        qDebug() << "GET: " << requestTokenUrl
                 << "with" << qoauth->consumerKey()
                           << qoauth->consumerSecret();

        QOAuth::ParamMap oAuthParams;
        oAuthParams.insert("oauth_callback", "oob");

        QOAuth::ParamMap reply = qoauth->requestToken(requestTokenUrl,
                                                      QOAuth::GET,
                                                      QOAuth::HMAC_SHA1,
                                                      oAuthParams);

        if (qoauth->error() == QOAuth::NoError)
        {
            qDebug() << "requestToken OK:" << reply.keys();

            token = reply.value(QOAuth::tokenParameterName());
            tokenSecret = reply.value(QOAuth::tokenSecretParameterName());

            qDebug() << "Token:" << token;
            qDebug() << "Token Secret:" << tokenSecret.left(5)
                     << "********** (hidden)";

            QUrl oAuthAuthorizeUrl(this->serverScheme + this->serverURL
                                   + "/oauth/authorize");
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
            oAuthAuthorizeUrl.addQueryItem("oauth_token", token);
#else
            QUrlQuery query;
            query.addQueryItem("oauth_token", token);
            oAuthAuthorizeUrl.setQuery(query);
#endif
            QDesktopServices::openUrl(oAuthAuthorizeUrl);

            // Send also a signal, so AccountDialog can show the URL in
            // a label, in case the browser didn't launch
            emit openingAuthorizeURL(oAuthAuthorizeUrl);

            // Now, user should enter VERIFIER in AccountDialog to authorize the program
        }
        else
        {
            qDebug() << "QOAuth error" << qoauth->error() << "!";
            qDebug() << reply.keys();

            emit authorizationFailed(tr("Authorization error"),
                                     tr("There was an OAuth error while trying "
                                        "to get the authorization token.")
                                     + "\n\n"
                                     + tr("QOAuth error %1").arg(qoauth->error()));
        }
    }
}



void PumpController::authorizeApplication(QString verifierCode)
{
    qDebug() << "Verifier code entered by user:" << verifierCode;

    QOAuth::ParamMap moreParams;
    moreParams.insert("oauth_verifier", verifierCode.toUtf8()); // verifier as QByteArray

    QString requestAuthorizationUrl = this->serverScheme + this->serverURL
                                      + "/oauth/access_token";

    QOAuth::ParamMap reply = qoauth->accessToken(requestAuthorizationUrl,
                                                 QOAuth::GET,
                                                 token,
                                                 tokenSecret,
                                                 QOAuth::HMAC_SHA1,
                                                 moreParams);

    if (qoauth->error() == QOAuth::NoError) // Woooohooo!!
    {
        qDebug() << "Got authorized token; Dianara is authorized to access the account";
        token = reply.value(QOAuth::tokenParameterName());
        tokenSecret = reply.value(QOAuth::tokenSecretParameterName());

        this->isApplicationAuthorized = true;

        this->showStatusMessageAndLogIt(tr("Application authorized successfully."));

        QSettings settings;
        settings.setValue("isApplicationAuthorized", this->isApplicationAuthorized);
        settings.setValue("token",       this->token);
        settings.setValue("tokenSecret", this->tokenSecret);
        settings.sync();

        qDebug() << "Token:" << token;
        qDebug() << "TokenSecret:" << tokenSecret;

        emit this->authorizationStatusChanged(isApplicationAuthorized);
    }
    else
    {
        this->showStatusMessageAndLogIt(tr("OAuth error while authorizing application.")
                                        + QString(" (%1)").arg(qoauth->error()));
        qDebug() << "OAuth error while authorizing application" << qoauth->error();
    }
}




/*
 * Called by a QTimer;
 * Get initial data (profile, contacts, timelines), one step at a time
 *
 */
void PumpController::getInitialData()
{
    qDebug() << "PumpController::getInitialData() step" << initialDataStep;

    initialDataTimer->setInterval(3000);  // Every 3 sec

    // If we're still waiting for a proxy password, don't do anything
    if (this->needsProxyPassword())
    {
        emit currentJobChanged(tr("Waiting for proxy password..."));
        return;
    }

    /*
     * FIXME: this needs to be way more elaborate.
     *
     * Ensure certain stuff has been received before continuing.
     *
     * For instance, getting the "following" list is needed before
     * being able to post to specific contacts, and to know the state
     * of following/not Following the author of a post in a timeline
     *
     */

    switch (this->initialDataStep)
    {
    case 0:
        if (!haveProfile)
        {
            this->getUserProfile(this->userId);
        }
        break;

    case 1:
        // Ensure we have the profile already, before getting contact lists...
        if (!haveProfile)
        {
            initialDataStep = -1; // Restart initialization!
            this->showStatusMessageAndLogIt(tr("Still waiting for profile. "
                                               "Trying again..."));
        }
        break;

    case 2:
        if (!haveFollowing)
        {
            if (haveProfile) // Require profile first, since it provides followingCount
            {
                this->getContactList("following");
            }
            else
            {
                --initialDataStep; // go back 1 to retry
            }
        }
        break;

    case 3:
        if (!haveFollowers)
        {
            if (haveProfile) // Required for followersCount
            {
                this->getContactList("followers");
            }
            else
            {
                --initialDataStep; // retry
            }
        }
        break;

    case 4:
        if (!haveMainTL)
        {
            this->getFeed(PumpController::MainTimelineRequest,
                          this->postsPerPageMain);
        }
        break;

    case 5:
        if (!haveMainMF)
        {
            this->getFeed(PumpController::MinorFeedMainRequest, 50);
        }
        break;

    case 6:
        if (!havePersonLists)
        {
            this->getListsList();
        }
        break;

    case 7:
        if (!haveDirectTL)
        {
            this->getFeed(PumpController::DirectTimelineRequest,
                          this->postsPerPageOther);
        }
        break;

    case 8:
        if (!haveDirectMF)
        {
            this->getFeed(PumpController::MinorFeedDirectRequest, 20);
        }
        break;

    case 9:
        if (!haveActivityTL)
        {
            this->getFeed(PumpController::ActivityTimelineRequest,
                          this->postsPerPageOther);
        }
        break;

    case 10:
        if (!haveActivityMF)
        {
            this->getFeed(PumpController::MinorFeedActivityRequest, 20);
        }
        break;

    case 11:
        if (!haveFavoritesTL)
        {
            this->getFeed(PumpController::FavoritesTimelineRequest,
                          this->postsPerPageOther);
        }
        break;

    case 12:
        // If some data is still missing, go back to the beginning of the cycle
        if (!haveProfile
         || !haveFollowing  || !haveFollowers || !havePersonLists
         || !haveMainTL     || !haveMainMF
         || !haveDirectTL   || !haveDirectMF
         || !haveActivityTL || !haveActivityMF
         || !haveFavoritesTL)
        {
            // Unless we've already tried several times, like people with broken
            // contact lists, for instance, which will never be received
            if (initialDataAttempts < 5)
            {
                initialDataStep = -1;
                ++initialDataAttempts;

                QString attempts;
                if (initialDataAttempts > 1)
                {
                    attempts = tr("%1 attempts").arg(initialDataAttempts);
                }
                else
                {
                    attempts = tr("1 attempt");
                }

                this->showStatusMessageAndLogIt(tr("Some initial data was not "
                                                   "received. Restarting "
                                                   "initialization...")
                                                + " (" + attempts + ")");
            }
            else
            {
                this->showStatusMessageAndLogIt(tr("Some initial data was not "
                                                   "received after several "
                                                   "attempts. Something might "
                                                   "be wrong with your server. "
                                                   "You might still be able to "
                                                   "use the service normally."));
            }
        }
        else
        {
            this->showStatusMessageAndLogIt(tr("All initial data received. "
                                               "Initialization complete."));
            // If there are no problems, this will just wait one interval,
            // so it takes longer for the final (default) step to arrive
        }
        break;


    default:
        initialDataTimer->stop();

        if (initialDataAttempts < 5) // with 5+ attempts, leave previous message
        {
            this->showStatusMessageAndLogIt(tr("Ready."));
        }

        emit initializationCompleted();

        qDebug() << "--------------------------------------";
        qDebug() << "-- All initial data loaded -----------";
        qDebug() << "--------------------------------------";
    }

    emit initializationStepChanged(initialDataStep);

    ++initialDataStep;
}





/*
 * Send a NOTE to the server
 *
 */
void PumpController::postNote(QMap<QString, QString> audienceMap,
                              QString postText, QString postTitle)
{
    qDebug() << "PumpController::postNote()";

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       PublishPostRequest);

    qDebug() << "Should be posting to:" << audienceMap;



    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "note");
    if (!postTitle.isEmpty())
    {
        jsonVariantObject.insert("displayName", postTitle);
    }
    //jsonVariantObject.insert("summary", "summary test");
    jsonVariantObject.insert("content", postText);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "post");
    jsonVariant.insert("object", jsonVariantObject);

    QList<QVariantList> audience = processAudience(audienceMap);
    jsonVariant.insert("to", audience.at(0));
    jsonVariant.insert("cc", audience.at(1));


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;

    nam.post(postRequest, data);
}


/*
 * Post media-type of object (image, audio, video, file)
 *
 * First, upload the file.
 * Then we get its ID in a signal, and create the post itself
 *
 */
QNetworkReply *PumpController::postMedia(QMap<QString, QString> audienceMap,
                                         QString postText, QString postTitle,
                                         QString mediaFilename, QString mediaType,
                                         QString mimeContentType)
{
    qDebug() << "PumpController::postMedia()" << mediaType;
    qDebug() << "Uploading" << mediaFilename << "with title:" << postTitle;

    // Store postTitle, postText, and audienceMap, then upload
    this->currentPostTitle = postTitle;
    this->currentPostDescription = postText;
    this->currentAudienceMap = audienceMap;
    this->currentPostType = mediaType;

    QNetworkReply *networkReply;
    networkReply = this->uploadFile(mediaFilename,
                                    mimeContentType,
                                    UploadMediaForPostRequest);
    return networkReply;
}


/*
 * Post Media, step 2: after getting the ID in the file upload request,
 * create the post itself
 *
 */
void PumpController::postMediaStepTwo(QString id)
{
    qDebug() << "PumpController::postMediaStepTwo()"
             <<  currentPostType << "ID:" << id;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       PublishPostRequest);


    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", this->currentPostType);
    jsonVariantObject.insert("id", id);

    QList<QVariantList> audience = processAudience(this->currentAudienceMap);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "post");
    jsonVariant.insert("object", jsonVariantObject);
    jsonVariant.insert("to", audience.at(0));
    jsonVariant.insert("cc", audience.at(1));


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;
    nam.post(postRequest, data);
}



/*
 * Second step for avatar upload.
 *
 * Post the image to Public
 *
 */
void PumpController::postAvatarStepTwo(QString id)
{
    qDebug() << "PumpController::postAvatarStepTwo() image ID:" << id;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       PublishAvatarRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "image");
    jsonVariantObject.insert("id", id);

    // audience, Cc: Public
    QVariantMap jsonVariantPublic;
    jsonVariantPublic.insert("objectType", "collection");
    jsonVariantPublic.insert("id", "http://activityschema.org/collection/public");

    QVariantList jsonVariantAudience;
    jsonVariantAudience.append(jsonVariantPublic);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "post");
    jsonVariant.insert("object", jsonVariantObject);
    jsonVariant.insert("cc", jsonVariantAudience); // Cc: Public


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;
    nam.post(postRequest, data);
}



/*
 * Update post contents (the object)
 *
 */
void PumpController::updatePost(QString id,
                                QString type,
                                QString content,
                                QString title)
{
    qDebug() << "PumpController::updatePost(), post ID:" << id;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       UpdatePostRequest);
    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("id", id);
    jsonVariantObject.insert("objectType", type);

    if (!title.isEmpty()) // FIXME: needs a way to remove titles...
    {
        jsonVariantObject.insert("displayName", title);
    }

    jsonVariantObject.insert("content", content);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb",   "update");
    jsonVariant.insert("object", jsonVariantObject);

    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;
    nam.post(postRequest, data);
}



/*
 * Like (favorite) a post, by its ID (URL)
 *
 */
void PumpController::likePost(QString postId, QString postType,
                              QString authorId, bool like)
{
    qDebug() << "PumpController::likePost() liking post" << postId;

    QNetworkRequest likeRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       LikePostRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", postType);
    jsonVariantObject.insert("id", postId);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", like ? "favorite":"unfavorite"); // like or unlike
    jsonVariant.insert("object", jsonVariantObject);


    if (this->silentLikes)
    {
        QVariantMap authorVariant;
        authorVariant.insert("objectType", "person");
        authorVariant.insert("id", "acct:" + authorId);

        QVariantList jsonAudienceTo;
        jsonAudienceTo.append(authorVariant);   // To: only the author
        jsonVariant.insert("to", jsonAudienceTo);
    }



    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "about to POST:" << data;
    nam.post(likeRequest, data);
}



void PumpController::addComment(QString comment,
                                QString postID,
                                QString postType)
{
    qDebug() << "PumpController::addComment() sending comment to this post:"
             << postID;

    QNetworkRequest commentRequest = this->prepareRequest(this->apiFeedUrl,
                                                          QOAuth::POST,
                                                          CommentPostRequest);

    QVariantMap jsonVariantInReplyTo;
    jsonVariantInReplyTo.insert("id", postID);
    jsonVariantInReplyTo.insert("objectType", postType);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "comment");
    jsonVariantObject.insert("content", comment);
    jsonVariantObject.insert("inReplyTo", jsonVariantInReplyTo);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "post");
    jsonVariant.insert("object", jsonVariantObject);

    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "about to POST:" << data;
    nam.post(commentRequest, data);
}


/*
 * Update comment contents (object)
 *
 * FIXME: This should be merged with :updatePost()
 *
 */
void PumpController::updateComment(QString id, QString content)
{
    qDebug() << "PumpController::updateComment(), comment ID:" << id;

    QNetworkRequest postRequest = this->prepareRequest(this->apiFeedUrl,
                                                       QOAuth::POST,
                                                       UpdateCommentRequest);
    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("id", id);
    jsonVariantObject.insert("objectType", "comment");
    jsonVariantObject.insert("content", content);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb",   "update");
    jsonVariant.insert("object", jsonVariantObject);

    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "About to POST:" << data;
    nam.post(postRequest, data);
}




void PumpController::sharePost(QString postID, QString postType)
{
    qDebug() << "PumpController::sharePost() sharing post" << postID;

    QNetworkRequest shareRequest = this->prepareRequest(this->apiFeedUrl,
                                                        QOAuth::POST,
                                                        SharePostRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", postType);
    jsonVariantObject.insert("id", postID);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "share");
    jsonVariant.insert("object", jsonVariantObject);


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "about to POST:" << data;
    nam.post(shareRequest, data);
}


void PumpController::unsharePost(QString postId, QString postType)
{
    qDebug() << "PumpController::unsharePost() unsharing post" << postId;

    QNetworkRequest unshareRequest = this->prepareRequest(this->apiFeedUrl,
                                                          QOAuth::POST,
                                                          UnsharePostRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", postType);
    jsonVariantObject.insert("id", postId);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "unshare");
    jsonVariant.insert("object", jsonVariantObject);


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "about to POST:" << data;
    nam.post(unshareRequest, data);
}



void PumpController::deletePost(QString postID, QString postType)
{
    qDebug() << "PumpController::deletePost() deleting post" << postID;

    QNetworkRequest deleteRequest = this->prepareRequest(this->apiFeedUrl,
                                                         QOAuth::POST,
                                                         DeletePostRequest);
    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", postType);
    jsonVariantObject.insert("id", postID);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "delete");
    jsonVariant.insert("object", jsonVariantObject);


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "about to POST:" << data;
    nam.post(deleteRequest, data);
}



/*
 * Check if a user ID exists in the corresponding server,
 * and if it is a Pump server.
 *
 * Used before actually following a contact.
 *
 */
void PumpController::followContact(QString address)
{
    // Don't do anything if another check+follow is already in progress
    if (!addressPendingToFollow.isEmpty())
    {
        // FIXME -- Show more clearly, a dialog box or something
        this->showStatusMessageAndLogIt(tr("Can't follow %1 at this time.",
                                           "%1 is a user ID")
                                        .arg("'" + address + "'")
                                        + " "
                                        + tr("Trying to follow %1.",
                                             "%1 is a user ID")
                                          .arg("'" + addressPendingToFollow) + "'");

        emit cannotFollowNow(address);

        return;
    }

    QString host = address.split('@').last();

    QString url = "https://" // TMP/FIXME: should retry with HTTP if it fails
                  + host
                  + "/.well-known/webfinger?resource="
                  + address;

    QNetworkRequest checkRequest = this->prepareRequest(url,
                                                        QOAuth::GET,
                                                        CheckContactRequest);

    emit currentJobChanged(tr("Checking address %1 before "
                              "following...").arg(address));

    this->addressPendingToFollow = address;

    this->addressCheckTimer->start(60000); // Take care of possible timeouts (1 min)
    this->addressCheckTimedOut = false;

    qDebug() << "About to verify webfinger ID:" << url;    
    this->addressCheckReply = nam.get(checkRequest);
}


/*
 * Add a contact to the /following list with their webfinger address
 *
 * Info for the newly added contact will come in the reply
 *
 */
void PumpController::followVerifiedContact(QString address)
{
    qDebug() << "PumpController::followVerifiedContact()" << address;

    QNetworkRequest followRequest = this->prepareRequest(this->apiFeedUrl,
                                                         QOAuth::POST,
                                                         FollowContactRequest);

    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "person");
    jsonVariantObject.insert("id", "acct:" + address);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "follow");
    jsonVariant.insert("object", jsonVariantObject);
    if (this->silentFollows) // In 'private' mode, address to the same as the object
    {
        QVariantList jsonAudienceTo;
        jsonAudienceTo.append(jsonVariantObject); // To: only the specific user
        jsonVariant.insert("to", jsonAudienceTo);
    }

    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "about to POST:" << data;
    nam.post(followRequest, data);
}



/*
 * Remove a contact from the /following list with their webfinger address
 *
 */
void PumpController::unfollowContact(QString address)
{
    qDebug() << "PumpController::unfollowContact()" << address;

    QNetworkRequest unfollowRequest = this->prepareRequest(this->apiFeedUrl,
                                                           QOAuth::POST,
                                                           UnfollowContactRequest);
    QVariantMap jsonVariantObject;
    jsonVariantObject.insert("objectType", "person");
    jsonVariantObject.insert("id", "acct:" + address);

    QVariantMap jsonVariant;
    jsonVariant.insert("verb", "stop-following");
    jsonVariant.insert("object", jsonVariantObject);
    if (this->silentFollows)
    {
        QVariantList jsonAudienceTo;
        jsonAudienceTo.append(jsonVariantObject);
        jsonVariant.insert("to", jsonAudienceTo);
    }


    QByteArray data = this->prepareJSON(jsonVariant);
    qDebug() << "about to POST:" << data;
    nam.post(unfollowRequest, data);
}


/*
 * If the QTimer times out, abort the connection manually,
 * since Qt doesn't handle this
 *
 */
void PumpController::onValidationTimeout()
{
    qDebug() << "Aborting user ID check due to timeout!";
    this->addressCheckReply->abort();
    this->addressCheckReply->deleteLater();

    this->addressCheckTimedOut = true;
}
