/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ASPERSON_H
#define ASPERSON_H

#include <QObject>
#include <QVariantMap>

#include <QDebug>

class ASPerson : public QObject
{
    Q_OBJECT

public:
    explicit ASPerson(QVariantMap personMap, QObject *parent = 0);
    ~ASPerson();

    void updateDataFromPerson(ASPerson *person);


    QString getId();
    static QString cleanupId(QString originalId);

    QString getName();
    QString getNameWithFallback();
    static QString makeNameIdString(QString userName, QString userId);
    QString getHometown();
    QString getBio();

    QString getAvatar();
    QString getUrl();

    QString getTooltipInfo();

    QString getOutboxLink();

    bool isFollowed();

    //int getFollowingCount();
    //int getFollowersCount();

    QString getCreatedAt();
    QString getupdatedAt();

signals:

public slots:

private:
    QString id;

    QString name;
    QString hometown;
    QString bio;

    QString avatar;
    QString url;

    QString outboxLink;

    QString followed;
    //int followingCount;
    //int followersCount;

    QString createdAt;
    QString updatedAt;
};

#endif // ASPERSON_H
