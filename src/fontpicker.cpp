/*
 *   This file is part of Dianara
 *   Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "fontpicker.h"

FontPicker::FontPicker(QString description,
                       QString initialFontString,
                       QWidget *parent) : QWidget(parent)
{
    QFont initialFont;
    initialFont.fromString(initialFontString);
    if (initialFont.family().isEmpty()) // Invalid, so use standard font
    {
        this->currentFont = QFont();
    }
    else // Valid, so use it
    {
        this->currentFont = initialFont;
    }


    this->descriptionLabel = new QLabel(description, this);
    descriptionLabel->setWordWrap(true);


    this->sampleLineEdit = new QLineEdit("AaBbCcDdEeFf", this);
    sampleLineEdit->setReadOnly(true);
    updateFontSample();


    this->button = new QPushButton(QIcon::fromTheme("list-add-font",
                                                    QIcon(":/images/list-add.png")),
                                   tr("Change..."),
                                   this);
    connect(button, SIGNAL(clicked()),
            this, SLOT(selectFont()));


    this->layout = new QHBoxLayout();
    layout->addWidget(descriptionLabel, 2);
    layout->addWidget(sampleLineEdit,   5);
    layout->addWidget(button,           1);
    this->setLayout(layout);

    qDebug() << "FontPicker created";
}

FontPicker::~FontPicker()
{
    qDebug() << "FontPicker destroyed";
}


void FontPicker::updateFontSample()
{
    this->sampleLineEdit->setFont(currentFont);
    this->sampleLineEdit->setText(currentFont.toString());
    this->sampleLineEdit->setCursorPosition(0);
}

QString FontPicker::getFontInfo()
{
    return this->currentFont.toString();
}



////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// SLOTS /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void FontPicker::selectFont()
{
    bool fontOk = false;

    QFont newFont = QFontDialog::getFont(&fontOk,
                                         this->currentFont,
                                         this,
                                         tr("Choose a font"));

    if (fontOk)
    {
        this->currentFont = newFont;
        updateFontSample();
    }
}
